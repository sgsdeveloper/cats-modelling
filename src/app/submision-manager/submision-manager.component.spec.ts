import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmisionManagerComponent } from './submision-manager.component';

describe('SubmisionManagerComponent', () => {
  let component: SubmisionManagerComponent;
  let fixture: ComponentFixture<SubmisionManagerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmisionManagerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmisionManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
