import { NavBarComponent } from './../nav-bar/nav-bar.component';
import { Component,  QueryList, ViewChildren, ViewChild, ElementRef, OnInit, Input, TemplateRef } from '@angular/core';
import {DecimalPipe, Time} from '@angular/common';
import {Observable, Subscription} from 'rxjs';
import {CountryService} from '../country.service';
import {Country} from '../country';
import {NgbdSortableHeader, SortEvent} from '../sortable.directive';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { NgbDateStruct, NgbCalendar, NgbDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { CookieService } from 'ngx-cookie';
import { SubmissionService } from '../services/submission.service';
import { saveAs } from 'file-saver';
import { environment } from 'src/environments/environment';
import { DataService } from '../services/data.service';
import { MatSort, MatPaginator, MatTableDataSource, MatFormFieldModule } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Template } from '@angular/compiler/src/render3/r3_ast';
import { truncateWithEllipsis } from '@amcharts/amcharts4/.internal/core/utils/Utils';
import * as XLSX from 'xlsx'
export interface PeriodicElement {
  Ref_ID: string;
  Sub_Date: Date;
  Sub_Time: Time;
  UW_Name: string;
  Broker_Name: string;
  Inception: Date;
  Expiry: Date;
  LoB: string;
  TAT: string;
  Duplicate: string;
  Modeling_Status: string;
  UW_Status: string;
  Binder_Status: string;
}

@Component({
  selector: 'app-submision-manager',
  templateUrl: './submision-manager.component.html',
  styleUrls: ['./submision-manager.component.css'],
  providers: [CountryService, DecimalPipe]

})



export class SubmisionManagerComponent implements OnInit {

  clickEventsubscription:Subscription;

  displayedColumns: string[] = ['Action','Ref_ID', 'Sub_Date', 'Sub_Time', 'UW_Name', 'Broker_Name', 'Inception', 'Expiry',
   'LoB', 'TAT', 'Duplicate', 'Modeling_Status','UW_Status' ];

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatFormFieldModule, {static: false}) formmodule: MatFormFieldModule;
  form: FormGroup;
  bindersform: FormGroup;
  LoggedInUserId: string;
  modeofsubmission: string;
  private _url = environment.apiUrl;
  state$: string;
  loginStatus; formErrors = false;
  loading; submitted = false;
  message: string;
  dataSource: MatTableDataSource<PeriodicElement[]>;
  headerdata:any;
  sampleTabledata:any;
  isCompleted:any;
  CurrentRefId:string;

  isBinderComments:boolean;


  constructor(public fb: FormBuilder, private modalService: NgbModal,
              public router: Router,
              public activatedRoute: ActivatedRoute,
              // tslint:disable-next-line: max-line-length
              private calendar: NgbCalendar, public service: CountryService, private http: HttpClient, private _cookieService: CookieService,
              public datepipe: DatePipe, private submission: SubmissionService, private dataservce: DataService) {
                this.clickEventsubscription=    this.dataservce.getClickEvent().subscribe(()=>{
                  this.getusersubmissionswithdates();
                  })


                this.form = this.fb.group({
      id: [''],
      policy_type: ['',[Validators.required]],
      uw_name: ['',[Validators.required]],
      inception_date: ['',[Validators.required]],
      expire_date: ['',[Validators.required]],
      tat: [''],
      comment: [''],
      lob: ['',[Validators.required]],
      broker: [''],
      SOVfile: [null],
      Instructionsfile: [null],

    });

    this.bindersform  = this.fb.group({
      id:[''],
      underwritingstatus: [''],
      comments: [''],
      boundpremium: [''],
      boundpremiumCurrency: [''],
      purepremium: [''],
      purepremiumCurrency: [''],
      Bindersfile: [null],
    });
                this.countries$ = service.countries$;
                this.total$ = service.total$;

  }

  countries$: Observable<Country[]>;
  total$: Observable<number>;
  closeResult: string;
  instructions = '';
  binders = '';
  catfilename = '';
  _instruction = '';
  _binders = '';
  _catfile = '';
  public _subission = null;
  public _lobs = null;
  public _userList  = null;
  public totalSubmissions : string;
  rowdata: Observable<any>;
  isBounded:boolean = false;
  expirationDate: NgbDateStruct;
  inceptionDate: NgbDateStruct;
  isBindersMandatoryFilled:boolean = true;
  date: { year: number, month: number };
  @ViewChild('catfileUploader', {static: false}) catfileUploader: ElementRef;
  @ViewChild('insfileUploader', {static: false}) insfileUploader: ElementRef;
  @ViewChild('bindersfileUploader', {static: false}) bindersfileUploader: ElementRef;

  @ViewChild('dp', {static: false}) dp: NgbDatepicker;
  @ViewChild('mymodal', {static: false}) mymodal: TemplateRef<any>;

  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;


  onSort({column, direction}: SortEvent) {

    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }


  open(content, data, action) {
    this.CurrentRefId = data.Ref_ID;
    this.modeofsubmission = action;
   // this.isCompleted = true;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg', backdrop: 'static'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      this.isCompleted = false;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      this.isCompleted = false;
    });

   // tslint:disable-next-line: align
   if (action === 'edit'){
    this.form.controls.id.setValue(data.Ref_No);
    this.form.controls.policy_type.setValue(data.Policy_Type);
    this.form.controls.uw_name.setValue(data.UW_Name);
    // tslint:disable-next-line: max-line-length
    this.inceptionDate = {year:+data.Inception.split('-')[0], month:+data.Inception.split('-')[1], day:+data.Inception.split('-')[2]} as NgbDateStruct;
    this.expirationDate = {year:+data.Expiry.split('-')[0], month:+data.Expiry.split('-')[1], day:+data.Expiry.split('-')[2]} as NgbDateStruct;
   // this.form.controls.inception_date.setValue('[{year:2018,month:3,day:28}]');
   // this.form.controls.expire_date.setValue(data.expire_date);
    this.form.controls.tat.setValue(data.TAT);
    this.form.controls.comment.setValue(data.UW_Comments);
    this.form.controls.lob.setValue(data.LoB);
    this.form.controls.broker.setValue(data.Broker_Name);
    this.submission.getdocumentIds(data.Ref_No).subscribe(resp => {
      if(resp[0].path.indexOf('\\') > 0){
        this.catfilename = (resp[0].path).split('\\')[(resp[0].path).split('\\').length - 1];
        this.instructions = (resp[1].path).split('\\')[(resp[1].path).split('\\').length - 1];
        this.form.controls.SOVfile.setValue((resp[0].path).split('\\')[(resp[0].path).split('\\').length - 1]);
        this.form.controls.Instructionsfile.setValue((resp[1].path).split('\\')[(resp[1].path).split('\\').length - 1]);
       }else{

        this.catfilename = (resp[0].path).split('/')[(resp[0].path).split('/').length - 1];
        this.instructions = (resp[1].path).split('/')[(resp[1].path).split('/').length - 1];
          this.form.controls.SOVfile.setValue((resp[0].path).split('/')[(resp[0].path).split('/').length - 1]);
          this.form.controls.Instructionsfile.setValue((resp[1].path).split('/')[(resp[1].path).split('/').length - 1]);
       }
    });
   }else if (action === 'view'){
    this.rowdata = data;
    this.form.controls.id.setValue(data.Ref_No);
    this.form.controls.policy_type.setValue(data.Policy_Type);
    this.form.controls.uw_name.setValue(data.UW_Name);
    this.form.controls.inception_date.setValue(data.Inception);
    this.form.controls.expire_date.setValue(data.Expiry);
    this.form.controls.tat.setValue(data.TAT);
    this.form.controls.comment.setValue(data.UW_Comments);
    this.form.controls.lob.setValue(data.LoB);
    this.form.controls.broker.setValue(data.Broker_Name);

    this.submission.getdocumentIds(data.Ref_No).subscribe(resp => {
      if(resp[0].path.indexOf('\\') > 0){
        this.catfilename = (resp[0].path).split('\\')[(resp[0].path).split('\\').length - 1];
        this.instructions = (resp[1].path).split('\\')[(resp[1].path).split('\\').length - 1];
        this.form.controls.SOVfile.setValue((resp[0].path).split('\\')[(resp[0].path).split('\\').length - 1]);
        this.form.controls.Instructionsfile.setValue((resp[1].path).split('\\')[(resp[1].path).split('\\').length - 1]);
       }else{

        this.catfilename = (resp[0].path).split('/')[(resp[0].path).split('/').length - 1];
        this.instructions = (resp[1].path).split('/')[(resp[1].path).split('/').length - 1];
          this.form.controls.SOVfile.setValue((resp[0].path).split('/')[(resp[0].path).split('/').length - 1]);
          this.form.controls.Instructionsfile.setValue((resp[1].path).split('/')[(resp[1].path).split('/').length - 1]);
       }
    });


   }else if(action === 'binders'){
    this.isBindersMandatoryFilled = true;
    this.rowdata = data;
    this.bindersform.controls.id.setValue(data.Ref_No);
    if(data.UW_Status === "Bound" || data.UW_Status === "Not Bound"){
      if(data.UW_Status === "Bound"){
        this.isBounded = false;
        this.submission.getdocumentIds(data.Ref_No).subscribe(resp => {
          if(resp[4].path.indexOf('\\') > 0){
            this.binders = ((resp[4].path).split('\\')[(resp[4].path).split('\\').length - 1]);
            }else{
              this.binders = ((resp[4].path).split('/')[(resp[4].path).split('/').length - 1]);
           }
        });
      }else{
        this.isBounded = true;
        this.binders = '';
      }
    this.bindersform.controls.underwritingstatus.setValue(data.UW_Status);
    this.isBindersMandatoryFilled = false;
    }else{
       this.isBounded = true;
      this.binders = '';
      this.bindersform.controls.underwritingstatus.setValue("select");
    }
    this.bindersform.controls.comments.setValue(data.Comments);
    this.bindersform.controls.boundpremium.setValue(data.Bound_Premium);
    this.bindersform.controls.boundpremiumCurrency.setValue(data.Bound_Premium_Currency);
    this.bindersform.controls.purepremium.setValue(data.Pure_Premium);
    this.bindersform.controls.purepremiumCurrency.setValue(data.Pure_Premium_Currency);

   }else{
     this.form.reset();
     this.inceptionDate = null;
     this.expirationDate = null;
   }

  }

  checkBindersMandatoryFilled(){
    if(this.bindersform.get('underwritingstatus').value == "Bound"){
      if(this.bindersform.get('boundpremium').value != null &&
         this.bindersform.get('boundpremiumCurrency').value != null &&
         this.bindersform.get('purepremium').value != null &&
         this.bindersform.get('purepremiumCurrency').value != null &&
         this.binders !== ''){
          this.isBindersMandatoryFilled = false;
      }else{
        this.isBindersMandatoryFilled = true;
      }

      if(this.bindersform.get('boundpremium').value == '' ||
      this.bindersform.get('boundpremiumCurrency').value == '' ||
      this.bindersform.get('purepremium').value == '' ||
      this.bindersform.get('purepremiumCurrency').value == ''){
       this.isBindersMandatoryFilled = true;
   }
    }else{
       if( this.bindersform.get('comments').value != null){
        this.isBindersMandatoryFilled = false;
      }else{
        this.isBindersMandatoryFilled = true;
      }
    }
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  submitBindersForm(){
    var formData: any = new FormData();
    formData.append('id', this.bindersform.get('id').value);
    formData.append('underwritingstatus', this.bindersform.get('underwritingstatus').value);
    formData.append('comments', this.bindersform.get('comments').value);
    formData.append('boundpremium', this.bindersform.get('boundpremium').value);
    formData.append('boundpremiumCurrency', this.bindersform.get('boundpremiumCurrency').value);
    formData.append('purepremium', this.bindersform.get('purepremium').value);
    formData.append('purepremiumCurrency', this.bindersform.get('purepremiumCurrency').value);
    if (this.bindersform.get('Bindersfile').value !== null &&  this.bindersform.get('underwritingstatus').value === "Bound"){
      formData.append('bindersfile', this.bindersform.get('Bindersfile').value);
    }

     this.submission.addBindersSubmission(formData).subscribe(resp => {
         this.getusersubmissionswithdates();
     })
     this.binders = '';
     this.bindersform.reset();
     this.modalService.dismissAll();
  }
  onStatusChange(){
    this.isBindersMandatoryFilled = true;
    let underwritingstatus = this.bindersform.get('underwritingstatus').value;
    if(underwritingstatus == 'Bound'){
      this.isBounded = false;
      this.bindersform.controls.comments.setValue('');
    }else{
      this.isBounded = true;
      this.bindersform.controls.comments.setValue('');
      this.bindersform.controls.boundpremium.setValue('');
      this.bindersform.controls.boundpremiumCurrency.setValue('');
      this.bindersform.controls.purepremium.setValue('');
      this.bindersform.controls.purepremiumCurrency.setValue('');
      this.binders = "";
    }
  }
  submitForm() {

    this.submitted = true;
    this.formErrors = false;
    if (this.form.invalid) {
      return;
    }

    var formData: any = new FormData();
    if(this.form.get('tat').value == null || this.form.get('tat').value == ''){
      this.form.controls.tat.setValue("Normal");
    }
    if(this.form.get('broker').value == null || this.form.get('broker').value == ''){
      this.form.controls.broker.setValue("");
    }
    if(this.form.get('comment').value == null || this.form.get('comment').value == ''){
      this.form.controls.comment.setValue("");
    }
    if (this.modeofsubmission === 'new'){

    formData.append('inception_date', this.inceptionDate.year + '-' + this.inceptionDate.month + '-' + this.inceptionDate.day);
    formData.append('expire_date', this.expirationDate.year + '-' + this.expirationDate.month + '-' + this.expirationDate.day);
    formData.append('policy_type', this.form.get('policy_type').value);
    formData.append('tat', this.form.get('tat').value);
    formData.append('broker', this.form.get('broker').value);
    formData.append('lob', this.form.get('lob').value);
    formData.append('comment', this.form.get('comment').value);
    formData.append('updated_by', this.form.get('uw_name').value);
    formData.append('sovfile', this.form.get('SOVfile').value);
    formData.append('instructionsfile', this.form.get('Instructionsfile').value);


    this.submission.addNewSubmission(formData).subscribe(resp => {
      if (resp.id >= 0){
        this.getusersubmissionswithdates();
      }else{
      }
    })
  }else{
    formData.append('id', this.form.get('id').value);
    formData.append('inception_date', this.inceptionDate.year + '-' + this.inceptionDate.month + '-' + this.inceptionDate.day);
    formData.append('expire_date', this.expirationDate.year + '-' + this.expirationDate.month + '-' + this.expirationDate.day);
    formData.append('policy_type', this.form.get('policy_type').value);
    formData.append('tat', this.form.get('tat').value);
    formData.append('broker', this.form.get('broker').value);
    formData.append('lob', this.form.get('lob').value);
    formData.append('comment', this.form.get('comment').value);
    formData.append('updated_by',this.form.get('uw_name').value);
    if (this.form.get('SOVfile').value !== null){
      formData.append('sovfile', this.form.get('SOVfile').value);
    }
    if (this.form.get('Instructionsfile').value !== null){
      formData.append('instructionsfile', this.form.get('Instructionsfile').value);
    }
    this.submission.editSubmission(formData).subscribe(resp => {

      this.getusersubmissionswithdates();

    })

  }

    //this.catfileUploader.nativeElement.value = null;
    //this.insfileUploader.nativeElement.value = null;
    this.form.reset();
    this.modalService.dismissAll();
  }


  openModellingFile(event: any, type) {
    if (type === 'catfile') {
      let _catmodellfile : HTMLElement = document.getElementById('catmodellfile') as HTMLElement;
      _catmodellfile.click();

    }else if (type === 'binders') {
      let _catbindersfile : HTMLElement = document.getElementById('bindersfile') as HTMLElement;
      _catbindersfile.click();

    } else {
      let _catInstructfile : HTMLElement = document.getElementById('catinstructfile') as HTMLElement;
    _catInstructfile.click();
    }

  }

  onFileChange(event: any, type) {
    if (type === 'catfile') {
      this.catfilename = event.target.files[0].name;
      const file = (event.target as HTMLInputElement).files[0];
      this.form.patchValue({
        SOVfile : file
      });
      this.form.get('SOVfile').updateValueAndValidity();
     }else if (type === 'binders') {
      this.binders = event.target.files[0].name;
      const file = (event.target as HTMLInputElement).files[0];
      this.bindersform.patchValue({
        Bindersfile : file
      });
      this.bindersform.get('Bindersfile').updateValueAndValidity();
      this.checkBindersMandatoryFilled();
     } else {
      this.instructions = event.target.files[0].name;
      const file = (event.target as HTMLInputElement).files[0];
      this.form.patchValue({
        Instructionsfile : file
        });
      this.form.get('Instructionsfile').updateValueAndValidity();
    }
  }
  AddInceptionDate(){
    this.expirationDate = {year:+this.inceptionDate.year+1,month:+this.inceptionDate.month,day:+this.inceptionDate.day};
  }
  addAccountName(ev,filename){
    if(filename === 'CAT'){
      document.getElementById('catfileAccountName').innerHTML = "";
      }
      else{
      document.getElementById('MIAccountName').innerHTML = ""
      }
    let workBook = null;
    let jsonData = null;
    const reader = new FileReader();
    const file = ev.target.files[0];
    reader.onload = (event) => {
      const data = reader.result;
      workBook = XLSX.read(data, { type: 'binary' });
      jsonData = workBook.SheetNames.reduce((initial, name) => {
        const sheet = workBook.Sheets[name];
        initial[name] = XLSX.utils.sheet_to_json(sheet);
        return initial;
      }, {});
      if(filename === 'CAT'){
      const dataString = JSON.stringify(jsonData.Report[0]['Account Name']);
      document.getElementById('catfileAccountName').innerHTML = "Account Name : "+dataString.split('"').join('');
      }
      else{
        const dataString = JSON.stringify(jsonData.Sheet1[0]['__EMPTY_1']);
      document.getElementById('MIAccountName').innerHTML = "Account Name : "+dataString.split('"').join('');

      }
    }
    reader.readAsBinaryString(file);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit() {
    this.LoggedInUserId = sessionStorage.getItem("LoggedInUserId");
    this.getusersubmissionswithdates();
    this._lobs = this.submission.getLobs();
    this._userList = this.submission.getUserList();
  }

  ngAfterViewInit() {

    this.state$ = this.activatedRoute.snapshot.paramMap.get('id');

    if(this.state$ != null){
      this.router.navigate(['/home/submission']);
      let currentTab='';
      this._subission.forEach(obj => {
        Object.entries(obj).forEach(([key, value]) => {
            if(`${value}` == this.state$){
              currentTab = obj;
            }
        });
      });
      this.open(this.mymodal,currentTab,'edit');
    }
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
}

  get f() {
    return this.form.controls;
  }



  getusersubmissionswithdates(){
    this.dataservce.currentMessage.subscribe(message => this.message = message);
    var dates = this.message.split(',');
    this.submission.getSubmissionswithdates(this.LoggedInUserId,dates[0],dates[1]).subscribe(resp => {
      this._subission = resp;
      this.dataSource = new MatTableDataSource<PeriodicElement[]>(this._subission);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
     });

 }

downloadFile(data, type){
this.submission.getdocumentIds(data.Ref_No).subscribe(resp => {
  if (type === 'SOV'){
     window.location.href= this._url+'/filedownload/'+resp[0].document_id;
     }else if (type === 'binders'){
      window.location.href= this._url+'/filedownload/'+resp[2].document_id;
      }else{
      window.location.href= this._url+'/filedownload/'+resp[1].document_id;
     }
     });
  }

  getCookie(key: string) {
    return this._cookieService.get(key);
  }



}
