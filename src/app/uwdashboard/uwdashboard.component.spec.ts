import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UwdashboardComponent } from './uwdashboard.component';

describe('UwdashboardComponent', () => {
  let component: UwdashboardComponent;
  let fixture: ComponentFixture<UwdashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UwdashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UwdashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
