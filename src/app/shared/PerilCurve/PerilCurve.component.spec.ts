/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { PerilCurveComponent } from './PerilCurve.component';

describe('PerilCurveComponent', () => {
  let component: PerilCurveComponent;
  let fixture: ComponentFixture<PerilCurveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerilCurveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PerilCurveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
