import { Component, Inject, NgZone, PLATFORM_ID } from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from '@amcharts/amcharts4-geodata/worldLow';
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import { isPlatformBrowser } from '@angular/common';
import am4geodata_usaLow from "@amcharts/amcharts4-geodata/usaLow";


import * as am4charts from '@amcharts/amcharts4/charts';
import { CookieService } from 'ngx-cookie';
import { DashboardService } from 'src/app/services/dashboard.service';

@Component({
  selector: 'app-PerilCurve',
  templateUrl: './PerilCurve.component.html',
  styleUrls: ['./PerilCurve.component.scss']
})
export class PerilCurveComponent {

  private chart: am4charts.XYChart;
  LoggedInUserId: string;
  imagedata:any;
  constructor(@Inject(PLATFORM_ID) private platformId, private zone: NgZone,private dashbordServicApi:DashboardService,  private _cookieService: CookieService) {
    this.LoggedInUserId = sessionStorage.getItem("LoggedInUserId");
    this.dashbordServicApi.getmapdata(this.LoggedInUserId,"All").subscribe(data => {
      this.imagedata = data;
    })

  }
  getCookie(key: string) {
    return this._cookieService.get(key);
  }

  // Run the function only in the browser
  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  onPerilChange(e){
    let selectedPeril = e.target.value;
    this.dashbordServicApi.getmapdata(this.LoggedInUserId,selectedPeril).subscribe(data => {
      this.imagedata = data;
      this.ngAfterViewInit();
    })
  }

  ngAfterViewInit() {
    // Chart code goes in here
    this.browserOnly(() => {

      setTimeout (() => {
      am4core.useTheme(am4themes_animated);

      let chart = am4core.create("porildiv", am4maps.MapChart);

            // Set map definition
      chart.geodata = am4geodata_usaLow;

      // Pre-zoom the map
      chart.homeZoomLevel = 3;
      chart.homeGeoPoint = {
        latitude: 39.14,
        longitude: -92.69
      };

      // Set projection
      chart.projection = new am4maps.projections.Miller();

      // Create map polygon series
      var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

      // Exclude Antartica
      polygonSeries.exclude = ["AQ"];
      chart.zoomControl = new am4maps.ZoomControl();

      // Make map load polygon (like country names) data from GeoJSON
      polygonSeries.useGeodata = true;
      // Place series
      var placeSeries = chart.series.push(new am4maps.MapImageSeries());

      placeSeries.data = this.imagedata;
      placeSeries.dataFields.value = "count";
      var place = placeSeries.mapImages.template;
      place.nonScaling = true;
      place.propertyFields.latitude = "Lat";
      place.propertyFields.longitude = "Lon";

      var circle = place.createChild(am4core.Circle);
      circle.fill = am4core.color("#e33");
      circle.propertyFields.opacity = "Avrg";
      circle.tooltipText = "{State_name} : {count}";

      let heat = placeSeries.heatRules.push({
        target: circle,
        property: "radius",
        min: 10,
        max: 30
      });


      var label = place.createChild(am4core.Label);
      label.padding(15, 15, 15, 15);
      label.propertyFields.text = "State_name";
      label.propertyFields.horizontalCenter = "right";
      label.propertyFields.verticalCenter = "middle";

      let heatLegend = chart.createChild(am4charts.HeatLegend);
      heatLegend.minColor = am4core.color("#F5DBCB");
      heatLegend.maxColor = am4core.color("#e33");
      heatLegend.series = placeSeries;
      heatLegend.minValue = 0.00;
      heatLegend.maxValue = 1.00;
      heatLegend.orientation = "vertical";
      heatLegend.marginLeft = am4core.percent(2);
      heatLegend.align = "left";
      heatLegend.valign = "middle";
      heatLegend.width = am4core.percent(100);
      heatLegend.height = am4core.percent(50);
     // heatLegend.markerCount = 3;
      heatLegend.markerContainer.width = 5;
      heatLegend.valueAxis.renderer.labels.template.fontSize = 9;
      heatLegend.valueAxis.renderer.minGridDistance = 45;
    }, 2000);

    });

  }

  ngOnDestroy() {
    // Clean up chart when the component is removed
    this.browserOnly(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }
}
