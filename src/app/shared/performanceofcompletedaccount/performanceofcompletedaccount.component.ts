import { Component, Inject, NgZone, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { CookieService } from 'ngx-cookie';
import { DashboardService } from '../../services/dashboard.service';
// amCharts imports
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { DataService } from 'src/app/services/data.service';
@Component({
  selector: 'app-performanceofcompletedaccount',
  templateUrl: './performanceofcompletedaccount.component.html',
  styleUrls: ['./performanceofcompletedaccount.component.scss']
})
export class PerformanceofcompletedaccountComponent {

  private chart: am4charts.XYChart;
  LoggedInUserId: string;
  LoggedInUserName: string;
  LoggedInUserRole:string;
  showDiv:any;
  Uw_Name = 'All';
  loB_Name = 'All';
  CatAnalyst_Name: any;
  st_date = 'All';
  end_date = 'All';
  closeResult: string;
  message: string;
  constructor(@Inject(PLATFORM_ID) private platformId,
                   private zone: NgZone,private dashbordServicApi:DashboardService,
                     private _cookieService: CookieService,private dataservce: DataService) {
    this.LoggedInUserId = sessionStorage.getItem("LoggedInUserId");
    this.LoggedInUserName = sessionStorage.getItem("LoggedInUserName");
    this.LoggedInUserRole = sessionStorage.getItem("LoggedInUserRole");

    if(this.LoggedInUserRole === 'CAT_MANAGER'){
    this.CatAnalyst_Name = 'All';
    }else{
      this.CatAnalyst_Name = this.LoggedInUserName;
    }
  }


  getCookie(key: string) {
    return this._cookieService.get(key);
  }


  // Run the function only in the browser
  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  applyFilters(uw_name,lob_name,CatAnalyst_name,st_date,end_date){
    this.showDiv = true;
    this.Uw_Name = uw_name;
    this.loB_Name = lob_name;
    this.CatAnalyst_Name = CatAnalyst_name;
    this.dashbordServicApi.getCMPerformanceCompletedAccount(this.LoggedInUserId,uw_name,lob_name,CatAnalyst_name,st_date,end_date).subscribe(data => {
      this.drawCharts(data);
    })
  }

  drawCharts(datavalues){
      am4core.useTheme(am4themes_animated);
      // Themes end

      let chart = am4core.create('performancediv', am4charts.XYChart)
      chart.colors.step = 2;
      chart.data = datavalues;
      chart.legend = new am4charts.Legend()
      chart.legend.position = 'top'
      chart.legend.paddingBottom = 20
      chart.legend.labels.template.maxWidth = 95


      let xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
      xAxis.dataFields.category = 'TAT'
      xAxis.renderer.cellStartLocation = 0.1
      xAxis.renderer.cellEndLocation = 0.9
      xAxis.renderer.grid.template.location = 0;
      xAxis.renderer.minGridDistance = 30;

      var valueAxis1 = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis1.title.text = "Count";
      valueAxis1.numberFormatter.numberFormat = "#.";


            // Create series
      var series1 = chart.series.push(new am4charts.ColumnSeries());
      series1.dataFields.valueY = "Met";
      series1.dataFields.categoryX = "TAT";
      series1.yAxis = valueAxis1;
      series1.name = "Met";
      series1.columns.template.events.on("hit",this.callChildMethod, this);

     // series1.tooltipText = "{categoryX}\n {valueY}";
      var valueLabel = series1.bullets.push(new am4charts.LabelBullet());
      valueLabel.label.text = "{valueY}";
      valueLabel.label.fontSize = 12;
      valueLabel.label.dy = -10;

      var series2 = chart.series.push(new am4charts.ColumnSeries());
      series2.dataFields.valueY = "Not Met";
      series2.dataFields.categoryX = "TAT";
      series2.name = "Not Met";
      //series2.tooltipText = "{name}\n[bold font-size: 20]${valueY}M[/]";
      var valueLabel2 = series2.bullets.push(new am4charts.LabelBullet());
      valueLabel2.label.text = "{valueY}";
      valueLabel2.label.fontSize = 12;
      valueLabel2.label.dy = -10;

      series1.columns.template.fill = am4core.color("#007bff");
      series2.columns.template.fill = am4core.color("#e83e8c");



  }

  ngAfterViewInit() {
    // Chart code goes in here
    this.showDiv = true;
    this.browserOnly(() => {
      this.dataservce.currentMessage.subscribe(message => this.message = message);
      var dates = this.message.split(',');
      this.st_date = dates[0];
      this.end_date = dates[1];
      if(this.LoggedInUserRole === 'CAT_MANAGER'){
      this.dashbordServicApi.getCMPerformanceCompletedAccount(this.LoggedInUserId,'All','All','All',this.st_date,this.end_date).subscribe(data => {
        this.drawCharts(data);
      })
    }else{
      this.dashbordServicApi.getCMPerformanceCompletedAccount(this.LoggedInUserId,'All','All',this.LoggedInUserName,this.st_date,this.end_date).subscribe(data => {
        this.drawCharts(data);
      })
    }
    });
  }

  ngOnDestroy() {
    // Clean up chart when the component is removed
    this.browserOnly(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });

  }

  callChildMethod(ev){
      this.showDiv = !this.showDiv;
      let seltype= ev.target.dataItem.categories.categoryX;
      setTimeout (() => {

      // tslint:disable-next-line: max-line-length
      this.dashbordServicApi.getCMPerformanceCompletedAccountDrillDown(this.LoggedInUserId,seltype,this.Uw_Name,this.loB_Name,this.CatAnalyst_Name,this.st_date,this.end_date).subscribe(data => {
        am4core.useTheme(am4themes_animated);
      let chart = am4core.create("subperformancediv", am4charts.XYChart);
        chart.data = data;
        chart.colors.step = 2;

      chart.legend = new am4charts.Legend()
      chart.legend.position = 'top'
      chart.legend.paddingBottom = 20
      chart.legend.labels.template.maxWidth = 95



      let xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
      xAxis.dataFields.category = 'Loc_Band'
      xAxis.renderer.cellStartLocation = 0.1
      xAxis.renderer.cellEndLocation = 0.9
      xAxis.renderer.grid.template.location = 0;
      xAxis.renderer.minGridDistance = 30;

      let yAxis = chart.yAxes.push(new am4charts.ValueAxis());
      yAxis.min = 0;
      yAxis.renderer.inside = false;
      yAxis.renderer.labels.template.disabled = false;

      function createSeries(value, name, color) {
          let series = chart.series.push(new am4charts.ColumnSeries())
          series.dataFields.valueY = value
          series.dataFields.categoryX = 'Loc_Band'
          series.name = name
          series.columns.template.fill = am4core.color(color);

          let bullet = series.bullets.push(new am4charts.LabelBullet())
          bullet.interactionsEnabled = false
          bullet.dy = -10;
          bullet.label.text = '{valueY}'
         // bullet.label.fill = am4core.color('#ffffff')

          return series;
      }




      createSeries('Met', 'Met',"#007bff");
      createSeries('NotMet', 'Not Met',"#e83e8c");
      })




    }, 1000);
  }

  backtoMain(){
    this.showDiv = !this.showDiv;
    this.dashbordServicApi.getCMPerformanceCompletedAccount(this.LoggedInUserId,this.Uw_Name,
            this.loB_Name,this.CatAnalyst_Name,this.st_date,this.end_date).subscribe(data => {
      this.drawCharts(data);
    })
  }

}
