import { Component, Inject, NgZone, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { CookieService } from 'ngx-cookie';
import { DashboardService } from '../../services/dashboard.service';
// amCharts imports
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { Subscription } from 'rxjs';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-Lineofbusiness',
  templateUrl: './Lineofbusiness.component.html',
  styleUrls: ['./Lineofbusiness.component.scss']
})
export class LineofbusinessComponent {

  private chart: am4charts.XYChart;
  LoggedInUserId: string;
  st_date = 'All';
  end_date = 'All';
  message: string;
  clickEventsubscription:Subscription;
  constructor(@Inject(PLATFORM_ID) private platformId, private zone: NgZone,
              private dashbordServicApi:DashboardService,  private _cookieService: CookieService,
              private dataservce: DataService) {
    this.LoggedInUserId = sessionStorage.getItem("LoggedInUserId");

  }
  getCookie(key: string) {
    return this._cookieService.get(key);
  }

  // Run the function only in the browser
  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  applyFilters(st_date,end_date){
    this.st_date = st_date;
    this.end_date = end_date;
    this.dashbordServicApi.getUWbylineofbusiness(this.LoggedInUserId,st_date,end_date).subscribe(data => {
      this.drawChart(data);
    })
  }

  ngAfterViewInit() {
    // Chart code goes in here
    this.browserOnly(() => {
      this.dataservce.currentMessage.subscribe(message => this.message = message);
      var dates = this.message.split(',');
      this.st_date = dates[0];
      this.end_date = dates[1];
      this.dashbordServicApi.getUWbylineofbusiness(this.LoggedInUserId,this.st_date,this.end_date).subscribe(data => {
        this.drawChart(data);
      })
    });
  }

  ngOnDestroy() {
    // Clean up chart when the component is removed
    this.browserOnly(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });

  }


  drawChart(datavalues){

      // start of bar
      am4core.useTheme(am4themes_animated);
      // Themes end

      let chart = am4core.create('bardiv', am4charts.XYChart)
      chart.colors.step = 2;

      chart.legend = new am4charts.Legend()
      chart.legend.position = 'top'
      chart.legend.paddingBottom = 20
      chart.legend.labels.template.maxWidth = 95

      let xAxis = chart.xAxes.push(new am4charts.CategoryAxis())
      xAxis.dataFields.category = 'LoB'
      xAxis.renderer.cellStartLocation = 0.1
      xAxis.renderer.cellEndLocation = 0.9
      xAxis.renderer.grid.template.location = 0;
      xAxis.renderer.minGridDistance = 30;

      var valueAxis1 = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis1.title.text = "Count";
      valueAxis1.numberFormatter.numberFormat = "#.";

      var valueAxis2 = chart.yAxes.push(new am4charts.ValueAxis());
      valueAxis2.title.text = "$ Mn";
      valueAxis2.renderer.opposite = true;
      valueAxis2.renderer.grid.template.disabled = true;

            // Create series
      var series1 = chart.series.push(new am4charts.ColumnSeries());
      series1.dataFields.valueY = "Count_Of_Ref_Id";
      series1.dataFields.categoryX = "LoB";
      series1.yAxis = valueAxis1;
      series1.name = "Count";
     // series1.tooltipText = "{categoryX}\n {valueY}";
     series1.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
      var valueLabel = series1.bullets.push(new am4charts.LabelBullet());
      valueLabel.label.text = "{valueY}";
      valueLabel.label.fontSize = 12;
      valueLabel.label.dy = -10;

      var series2 = chart.series.push(new am4charts.ColumnSeries());
      series2.dataFields.valueY = "Tiv";
      series2.dataFields.categoryX = "LoB";
      series2.yAxis = valueAxis2;
      series2.name = "Total Insured Value";
      series2.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
      //series2.tooltipText = "{name}\n[bold font-size: 20]${valueY}M[/]";
      var valueLabel2 = series2.bullets.push(new am4charts.LabelBullet());
      valueLabel2.label.text = "$ {valueY}";
      valueLabel2.label.fontSize = 12;
      valueLabel2.label.dy = -10;
      chart.data = datavalues;
  }
}
