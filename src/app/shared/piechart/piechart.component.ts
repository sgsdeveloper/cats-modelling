import { Component, Inject, NgZone, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { CookieService } from 'ngx-cookie';
// amCharts imports
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { DashboardService } from 'src/app/services/dashboard.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-piechart',
  templateUrl: './piechart.component.html',
  styleUrls: ['./piechart.component.css']
})
export class PiechartComponent {

  private chart: am4charts.XYChart;
  LoggedInUserId: string;
  LoggedInUserName: string;
  LoggedInUserRole:string;
  st_date = 'All';
  end_date = 'All';
  message: string;
  constructor(@Inject(PLATFORM_ID) private platformId, private zone: NgZone,
            private dashbordServicApi:DashboardService,  private _cookieService: CookieService,
            private dataservce: DataService) {
    this.LoggedInUserId = sessionStorage.getItem("LoggedInUserId");
    this.LoggedInUserName = sessionStorage.getItem("LoggedInUserName");
    this.LoggedInUserRole = sessionStorage.getItem("LoggedInUserRole");
  }

  getCookie(key: string) {
    return this._cookieService.get(key);
  }
  // Run the function only in the browser
  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }

  ngAfterViewInit() {
    // Chart code goes in here
    this.browserOnly(() => {
      this.dataservce.currentMessage.subscribe(message => this.message = message);
      var dates = this.message.split(',');
      this.st_date = dates[0];
      this.end_date = dates[1];
      if(this.LoggedInUserRole === 'CAT_MANAGER'){
      this.dashbordServicApi.getpiechartdata(this.LoggedInUserId,'All','All','All',this.st_date,this.end_date).subscribe(data => {
        this.drawChart(data);
      })
    }else{
      this.dashbordServicApi.getpiechartdata(this.LoggedInUserId,'All','All',this.LoggedInUserName,this.st_date,this.end_date).subscribe(data => {
        this.drawChart(data);
      })
    }

    });
  }

  applyFilters(uw_name,lob_name,CatAnalyst_name,st_date,end_date){
    this.dashbordServicApi.getpiechartdata(this.LoggedInUserId,uw_name,lob_name,CatAnalyst_name,st_date,end_date).subscribe(data => {
      this.drawChart(data);
    })
  }

  drawChart(dataValues){

    am4core.useTheme(am4themes_animated);
    var chart = am4core.create("piediv", am4charts.PieChart);
    chart.data = dataValues;
    chart.legend = new am4charts.Legend();
    chart.legend.position = 'bottom';
    chart.legend.fontSize = 10;

    // Add and configure Series
    var pieSeries = chart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = "value";
    pieSeries.dataFields.category = "Name";
    pieSeries.slices.template.stroke = am4core.color("#fff");
    pieSeries.slices.template.strokeWidth = 2;
    pieSeries.slices.template.strokeOpacity = 1;
    pieSeries.colors.list = [
       am4core.color('#007bff'),
       am4core.color('#e83e8c'),
       am4core.color('#6f42c1'),
  ]

    // This creates initial animation
    pieSeries.hiddenState.properties.opacity = 1;
    pieSeries.hiddenState.properties.endAngle = -90;
    pieSeries.hiddenState.properties.startAngle = -90;
    pieSeries.ticks.template.disabled = true;
    pieSeries.alignLabels = false;
    pieSeries.labels.template.text = "{value.percent.formatNumber('#.0')}%";
    pieSeries.labels.template.radius = am4core.percent(-40);
    pieSeries.labels.template.fill = am4core.color("white");
  }

  ngOnDestroy() {
    // Clean up chart when the component is removed
    this.browserOnly(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }
}

