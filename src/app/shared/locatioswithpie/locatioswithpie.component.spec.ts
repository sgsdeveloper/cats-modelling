/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { LocatioswithpieComponent } from './locatioswithpie.component';

describe('LocatioswithpieComponent', () => {
  let component: LocatioswithpieComponent;
  let fixture: ComponentFixture<LocatioswithpieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocatioswithpieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocatioswithpieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
