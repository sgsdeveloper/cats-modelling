import { Component, Inject, NgZone, OnInit, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { CookieService } from 'ngx-cookie';
import { DashboardService } from '../../services/dashboard.service';
// amCharts imports
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { DataService } from 'src/app/services/data.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-locatioswithpie',
  templateUrl: './locatioswithpie.component.html',
  styleUrls: ['./locatioswithpie.component.scss']
})
export class LocatioswithpieComponent {

  private chart: am4charts.XYChart;
  LoggedInUserId: string;
  isClicked = false;
  st_date = 'All';
  end_date = 'All';
  message: string;
  clickEventsubscription:Subscription;
  constructor(@Inject(PLATFORM_ID) private platformId, private zone: NgZone,
              private dashbordServicApi : DashboardService,  private _cookieService: CookieService,
               private dataservce: DataService) {
    this.LoggedInUserId = sessionStorage.getItem("LoggedInUserId");

    }

 // Run the function only in the browser
 browserOnly(f: () => void) {
  if (isPlatformBrowser(this.platformId)) {
    this.zone.runOutsideAngular(() => {
      f();
    });
  }
}
getCookie(key: string) {
  return this._cookieService.get(key);
}

applyFilters(st_date,end_date){
  this.st_date = st_date;
  this.end_date = end_date;
  this.dashbordServicApi.getUWaccountsbylocation(this.LoggedInUserId,st_date,end_date).subscribe(data => {
    this.drawChart(data);
  })
}

ngAfterViewInit() {
  this.browserOnly(() => {
    this.dataservce.currentMessage.subscribe(message => this.message = message);
      var dates = this.message.split(',');
      this.st_date = dates[0];
      this.end_date = dates[1];
    this.dashbordServicApi.getUWaccountsbylocation(this.LoggedInUserId,this.st_date,this.end_date).subscribe(data => {
      this.drawChart(data);
    })
});
}
drawChart(datavalues){
  document.getElementById("locationswithpiechart").innerHTML = "";
  am4core.useTheme(am4themes_animated);
  let chart = am4core.create("locationswithpie", am4charts.XYChart);
  chart.data = datavalues;
  let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
  categoryAxis.dataFields.category = "Loc_Band";
  categoryAxis.renderer.grid.template.location = 0;
  categoryAxis.renderer.minGridDistance = 30;
  categoryAxis.title.text = "No of Locations";

  let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
  valueAxis.renderer.inside = false;
  valueAxis.renderer.labels.template.disabled = false;
  valueAxis.min = 0;
  valueAxis.title.text = "Count";

  let series = chart.series.push(new am4charts.ColumnSeries());
  series.dataFields.valueY = "Count_Of_Ref_Id";
  series.dataFields.categoryX = "Loc_Band";
  series.name = "Count_Of_Ref_Id";
  series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
 // series.columns.template.fillOpacity = .8;
  series.columns.template.fill = am4core.color("#007bff");

  var valueLabel2 = series.bullets.push(new am4charts.LabelBullet());
  valueLabel2.label.text = "{valueY}";
  valueLabel2.label.fontSize = 12;
  valueLabel2.label.dy = -10;

  let columnTemplate = series.columns.template;
  columnTemplate.strokeWidth = 2;
  columnTemplate.strokeOpacity = 1;

  series.columns.template.events.on("hit",this.callChildMethod, this);
}



callChildMethod(ev){
  var option;
  if(ev.target.dataItem.categories.categoryX == "< 500"){
    option="less";
  }else{
    option="greater";
  }
  am4core.useTheme(am4themes_animated);
  var chart = am4core.create("locationswithpiechart", am4charts.PieChart);
  this.dashbordServicApi.getUWaccountsbylocationdrilldown(this.LoggedInUserId,option,this.st_date,this.end_date).subscribe(data => {
    chart.data = data;
  })
  chart.legend = new am4charts.Legend();
  chart.legend.position = 'bottom';
  chart.legend.fontSize = 10;

  // Add and configure Series
  var pieSeries = chart.series.push(new am4charts.PieSeries());
  pieSeries.dataFields.value = "Count_Of_Ref_Id";
  pieSeries.dataFields.category = "Loc_Band";
  pieSeries.slices.template.stroke = am4core.color("#fff");
  pieSeries.slices.template.strokeWidth = 2;
  pieSeries.slices.template.strokeOpacity = 1;
//    pieSeries.colors.list = [
//      am4core.color('#FBC02D'),
//      am4core.color('#0288D1'),
//      am4core.color('#F44336'),
//      am4core.color('#8E24AA'),
// ]

  // This creates initial animation
  pieSeries.hiddenState.properties.opacity = 1;
  pieSeries.hiddenState.properties.endAngle = -90;
  pieSeries.hiddenState.properties.startAngle = -90;

  pieSeries.ticks.template.disabled = true;
  pieSeries.alignLabels = false;
  pieSeries.labels.template.text = "{value.percent.formatNumber('#.0')}%";
  pieSeries.labels.template.radius = am4core.percent(-40);
  pieSeries.labels.template.fill = am4core.color("white");

}

}
