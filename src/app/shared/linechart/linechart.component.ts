import { Component, Inject, NgZone, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { CookieService } from 'ngx-cookie';
import { DashboardService } from '../../services/dashboard.service';
// amCharts imports
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-linechart',
  templateUrl: './linechart.component.html',
  styleUrls: ['./linechart.component.scss']
})
export class LinechartComponent {

  private chart: am4charts.XYChart;
  LoggedInUserId: string;
  LoggedInUserName: string;
  st_date = 'All';
  end_date = 'All';
  message: string;
  constructor(@Inject(PLATFORM_ID) private platformId, private zone: NgZone,
              private dashbordServicApi:DashboardService,  private _cookieService: CookieService,private dataservce: DataService) {
    this.LoggedInUserId = sessionStorage.getItem("LoggedInUserId");
    this.LoggedInUserName = sessionStorage.getItem("LoggedInUserName");

  }
  getCookie(key: string) {
    return this._cookieService.get(key);
  }

  // Run the function only in the browser
  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }
  applyFilters(st_date,end_date){
    this.st_date = st_date;
    this.end_date = end_date;
    this.dashbordServicApi.getperilcurvedata(this.LoggedInUserId,st_date,end_date).subscribe(data => {
      this.drawChart(data);
    })
  }

  ngAfterViewInit() {
    // Chart code goes in here
    this.browserOnly(() => {
      this.dataservce.currentMessage.subscribe(message => this.message = message);
      var dates = this.message.split(',');
      this.st_date = dates[0];
      this.end_date = dates[1];
      this.dashbordServicApi.getperilcurvedata(this.LoggedInUserId,this.st_date,this.end_date).subscribe(data => {
        this.drawChart(data);
      })
    });
  }

  ngOnDestroy() {
    // Clean up chart when the component is removed
    this.browserOnly(() => {
      if (this.chart) {
        this.chart.dispose();
      }
    });
  }

  drawChart(datavalues){

// start of bar
am4core.useTheme(am4themes_animated);
// Themes end

let chart = am4core.create("usdiv", am4charts.XYChart);
chart.hiddenState.properties.opacity = 0; // this makes initial fade in effect
chart.legend = new am4charts.Legend();
chart.legend.useDefaultMarker = true;
chart.data = datavalues;

let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.renderer.grid.template.location = 0;
categoryAxis.dataFields.category = "name";
categoryAxis.renderer.minGridDistance = 40;

let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

let series = chart.series.push(new am4charts.CurvedColumnSeries());
series.dataFields.categoryX = "name";
series.fill=am4core.color("#696ad9");
series.dataFields.valueY = "total";
series.tooltipText = "$ {valueY.value}"
series.columns.template.strokeOpacity = 0;
series.clustered = false;
series.name = "All UnderWriters";

let series2 = chart.series.push(new am4charts.CurvedColumnSeries());
series2.dataFields.categoryX = "name";
series2.fill=am4core.color("#bdc3f5");
series2.dataFields.valueY = "value";
series2.tooltipText = "$ {valueY.value}"
series2.columns.template.strokeOpacity = 0;
series2.clustered = false;
series2.name = this.LoggedInUserName;
chart.cursor = new am4charts.XYCursor();
chart.cursor.maxTooltipDistance = 0;



series.dataItems.template.adapter.add('width', (width, target) => {
  return am4core.percent(target.valueY / valueAxis.max * 100);
})

series2.dataItems.template.adapter.add('width', (width, target) => {
  return am4core.percent(target.valueY / valueAxis.max * 100);
})


series.columns.template.events.on('parentset', function(event){
  event.target.zIndex = valueAxis.max - event.target.dataItem.values.valueY.value;
})

series2.columns.template.events.on('parentset', function(event){
  event.target.parent = series.columnsContainer;
  event.target.zIndex = valueAxis.max - event.target.dataItem.values.valueY.value;
})



  }

}
