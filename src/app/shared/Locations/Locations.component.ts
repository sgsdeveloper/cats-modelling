import { Component, Inject, NgZone, OnInit, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { CookieService } from 'ngx-cookie';
import { DashboardService } from '../../services/dashboard.service';
// amCharts imports
import * as am4core from '@amcharts/amcharts4/core';
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-Locations',
  templateUrl: './Locations.component.html',
  styleUrls: ['./Locations.component.scss']
})
export class LocationsComponent {

  private chart: am4charts.XYChart;
  LoggedInUserId: string;
  LoggedInUserName:string;
  LoggedInUserRole:string;
  st_date = 'All';
  end_date = 'All';
  message: string;
  constructor(@Inject(PLATFORM_ID) private platformId, private zone: NgZone,
              private dashbordServicApi:DashboardService,  private _cookieService: CookieService,
              private dataservce: DataService) {

    this.LoggedInUserId = sessionStorage.getItem("LoggedInUserId");
    this.LoggedInUserName = sessionStorage.getItem("LoggedInUserName");
    this.LoggedInUserRole = sessionStorage.getItem("LoggedInUserRole");

  }


  // Run the function only in the browser
  browserOnly(f: () => void) {
    if (isPlatformBrowser(this.platformId)) {
      this.zone.runOutsideAngular(() => {
        f();
      });
    }
  }
  getCookie(key: string) {
    return this._cookieService.get(key);
  }

  applyFilters(uw_name,lob_name,CatAnalyst_name,st_date,end_date){
    this.st_date = st_date;
    this.end_date = end_date;
    this.dashbordServicApi.getCMaccountsbylocation(this.LoggedInUserId,uw_name,lob_name,CatAnalyst_name,st_date,end_date).subscribe(data => {
      this.drawCharts(data);
    })
  }

  ngAfterViewInit() {
    this.browserOnly(() => {
      this.dataservce.currentMessage.subscribe(message => this.message = message);
      var dates = this.message.split(',');
      this.st_date = dates[0];
      this.end_date = dates[1];
      if(this.LoggedInUserRole === 'CAT_MANAGER'){
      this.dashbordServicApi.getCMaccountsbylocation(this.LoggedInUserId,'All','All','All',this.st_date,this.end_date).subscribe(data => {
        this.drawCharts(data);
      })
    }else{
      this.dashbordServicApi.getCMaccountsbylocation(this.LoggedInUserId,'All','All',this.LoggedInUserName,this.st_date,this.end_date).subscribe(data => {
        this.drawCharts(data);
      })
    }
  });
  }


  drawCharts(dataValues){
    am4core.useTheme(am4themes_animated);
    let chart = am4core.create("chartdiv", am4charts.XYChart);
    chart.data = dataValues;
    let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "Loc_Band";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 30;
    categoryAxis.title.text = "No of Locations";

    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.renderer.inside = false;
    valueAxis.renderer.labels.template.disabled = false;
    valueAxis.min = 0;
    valueAxis.title.text = "Count";

    let series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueY = "Count_Of_Ref_Id";
    series.dataFields.categoryX = "Loc_Band";
    series.name = "Count_Of_Ref_Id";
    series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
   // series.columns.template.fillOpacity = .8;
    series.columns.template.fill = am4core.color("#007bff");

    var valueLabel2 = series.bullets.push(new am4charts.LabelBullet());
    valueLabel2.label.text = "{valueY}";
    valueLabel2.label.fontSize = 12;
    valueLabel2.label.dy = -10;
  }

}
