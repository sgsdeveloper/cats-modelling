import {Country} from './country';

export const COUNTRIES: Country[] = [
  {
	ref_id:'string',
	sub_date:'string',
	sub_time:'string',
	UWName:'string',
	broker:'string',
	inception_date:'string',
	expire_date:'string',
	lob:'string',
	tat:'string',
	duplicate:'string',
	status:'Completed',
  submitted_at:'csdcdcdscdsvdsvdsvdv'
  },
  {

	ref_id:'string',
	sub_date:'string',
	sub_time:'string',
	UWName:'string',
	broker:'string',
	inception_date:'string',
	expire_date:'string',
	lob:'string',
	tat:'string',
	duplicate:'string',
  status:'On Hold',
  submitted_at:'csdcdcdscdsvdsvdsvdv'
  },
  {
	ref_id:'string',
	sub_date:'string',
	sub_time:'string',
	UWName:'string',
	broker:'string',
	inception_date:'string',
	expire_date:'string',
	lob:'string',
	tat:'string',
	duplicate:'string',
  status:'string',
  submitted_at:'csdcdcdscdsvdsvdsvdv'
  },
  {

	ref_id:'string',
	sub_date:'string',
	sub_time:'string',
  UWName:'string',
	broker:'string',
	inception_date:'string',
	expire_date:'string',
	lob:'string',
	tat:'string',
	duplicate:'string',
  status:'string',
  submitted_at:'csdcdcdscdsvdsvdsvdv'
  },
  {

	ref_id:'two',
	sub_date:'string',
	sub_time:'string',
	UWName:'string',
	broker:'string',
	inception_date:'string',
	expire_date:'string',
	lob:'string',
	tat:'string',
	duplicate:'N',
  status:'Details Required',
  submitted_at:'csdcdcdscdsvdsvdsvdv'
  }
];
