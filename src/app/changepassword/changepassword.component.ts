import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {
  LoggedInUserId:string;
  LoggedInUserRole:string;
  form: FormGroup;

  constructor(private _cookieService: CookieService,
    private loginServiceApi:LoginService, public fb: FormBuilder, public router: Router) {

      this.form = this.fb.group({
        currentPassword: [''],
        newPassword: [''],
        reenterPassword: ['']
      });
     }

  ngOnInit() {
    this.LoggedInUserId = sessionStorage.getItem("LoggedInUserId");
    this.LoggedInUserRole = sessionStorage.getItem("LoggedInUserRole");
  }

  getCookie(key: string) {
    return this._cookieService.get(key);
  }

  changePassword(){
    var formData: any = new FormData();
    formData.append('user_id', this.LoggedInUserId);
    formData.append('password', this.form.get('reenterPassword').value);
    this.loginServiceApi.getchangePassword(formData).subscribe(data => {
      alert("Password Changed Successfully");

    });
  }

  gotoHome(){
    if(this.LoggedInUserRole === 'CAT_MANAGER'){
    this.router.navigate(['/manager']);
    }else if(this.LoggedInUserRole === 'ANALYST'){
      this.router.navigate(['/analyst']);
    }else{
      this.router.navigate(['/home']);
    }

  }

}
