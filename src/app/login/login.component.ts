import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from '../services/login.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BroadcastService } from '../broadcast.service';
import { CookieService } from 'ngx-cookie';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  isUserLoggedIn = false;
  loginForm: FormGroup;
  loginStatus; formErrors = false;
  loading; submitted = false;
  constructor(private formBuilder: FormBuilder,private loginServiceApi:LoginService, public router: Router,
    private broadcastService: BroadcastService, private _cookieService: CookieService,) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });

    var cookeyValue = sessionStorage.getItem("userLoggineIn");
    if (cookeyValue && cookeyValue === 'true' && localStorage.isLoggedIn === 'true') {
      this.router.navigate(['/dashboard']);

    }

  }
  get f() {
    return this.loginForm.controls;
  }

  onSubmit(formData:string) {

    this.submitted = true;
    this.formErrors = false;
    if (this.loginForm.invalid) {
      return;
    }

    else{
this.loginServiceApi.checkLogin(formData).subscribe(resp=>{
  if(resp.User_id >= 0){
    sessionStorage.setItem('LoggedInUserId', resp.User_id);
    sessionStorage.setItem('LoggedInUserId', resp.User_id);
    sessionStorage.setItem('LoggedInUserName', resp.full_name);
    sessionStorage.setItem('LoggedInUserRole', resp.Role);

    if(resp.Rows == null){
      sessionStorage.setItem('NUMBEROFRECORDS', "10");
    }
    if(resp.Role === 'CAT_MANAGER'){
    if(resp.Rows == null){
      sessionStorage.setItem('NUMBEROFRECORDS', "10");
    }else{
      sessionStorage.setItem('NUMBEROFRECORDS', resp.Rows);
    }
    this.router.navigate(['/manager']);
    }else if(resp.Role === 'ANALYST'){
      this.router.navigate(['/analyst']);
    }else{
      this.router.navigate(['/home']);
    }

   } else{

  alert(resp.Status);

  }

})
  }

    this.loading = true;
    this.loginStatus = false;

    let data = { id: 1, name: 'thangappa', role: { name: 'UnderWriter' } };
    let broadcastObj: any = {};
    broadcastObj.userId = data.id;

    broadcastObj.isUserLoggedIn = true;
    sessionStorage.setItem('userLoggineIn', 'true');
    this.broadcastService.confirmLoggedIn(broadcastObj);


//    this.router.navigate(['/submission']);
    if (data !== null && data.role.name === 'UnderWriter') {
    } else if (data !== null && data.role.name === 'Analyst') {
    } else if (data !== null && data.role.name === 'Manager') {
    } else {

    }
  }

  getCookie(key: string) {
    return this._cookieService.get(key);
  }

}
