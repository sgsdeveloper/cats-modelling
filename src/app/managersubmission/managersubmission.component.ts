import { Component, OnInit, TemplateRef } from '@angular/core';

import { NavBarComponent } from './../nav-bar/nav-bar.component';
import { QueryList, ViewChildren, ViewChild, ElementRef, Input } from '@angular/core';
import {DecimalPipe} from '@angular/common';
import {Observable, Subscription} from 'rxjs';
import {CountryService} from '../country.service';
import {Country} from '../country';
import {NgbdSortableHeader, SortEvent} from '../sortable.directive';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { NgbDateStruct, NgbCalendar, NgbDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { CookieService } from 'ngx-cookie';
import { SubmissionService } from '../services/submission.service';
import { saveAs } from 'file-saver';
import { environment } from 'src/environments/environment';
import { DataService } from '../services/data.service';
import { MatSort, MatPaginator, MatTableDataSource, MatFormFieldModule } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-managersubmission',
  templateUrl: './managersubmission.component.html',
  styleUrls: ['./managersubmission.component.scss'],
  providers: [CountryService, DecimalPipe]
})
export class ManagersubmissionComponent implements OnInit {
  clickEventsubscription:Subscription;

  displayedColumns: string[] = ['Action','Cat_Manager', 'Cat_Analyst',
                                 'Ref_ID', 'Assigned','TAT','Sub_Date', 'Sub_Time', 'UW_Name',
                                  'UW_Comments', 'Modeling_Status','UW_Status'];

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatFormFieldModule, {static: false}) formmodule: MatFormFieldModule;

  form: FormGroup;
  LoggedInUserId: string;
  LoggedInUserName:string;
  modeofsubmission: string;
  private _url = environment.apiUrl;
  ouputfiletype:string;
  loginStatus; formErrors = false;
  loading; submitted = false;
  message: string;
  dataSource: MatTableDataSource<Country>;
  LoggedInUserRole: string;
  showQCChecks = true;
  isAssigned = true;
  QCCheckvariable:any;
  duplicateCheck:any;
  GeoCodeCheck:any;
  outputFileCreation:any;
  state$: string;
  constructor(public fb: FormBuilder, private modalService: NgbModal,
              public activatedRoute: ActivatedRoute,
              public router: Router,
              // tslint:disable-next-line: max-line-length
              private calendar: NgbCalendar, public service: CountryService, private http: HttpClient, private _cookieService: CookieService,
              public datepipe: DatePipe, private submission: SubmissionService, private dataservce: DataService) {


                this.clickEventsubscription=    this.dataservce.getClickEvent().subscribe(()=>{
                  this.getusersubmissionswithdates();
                  })

                this.form = this.fb.group({
      id: [''],
      policy_type: [''],
      uw_name: ['', [Validators.required]],
      inception_date: ['', [Validators.required]],
      expire_date: ['', [Validators.required]],
      tat: [''],
      comment: [''],
      lob: ['', [Validators.required]],
      broker: [''],
      SOVfile: [null],
      Instructionsfile: [null],
      assignanalyst : [''],
      subline: [''],
      cacomments: [''],
      email_id: [''],
      email_body: [''],
      sov_file_validation: [''],
      duplicate_validation: [''],
      geo_validation: [''],
     output_validation: ['']


    });

                this.countries$ = service.countries$;
                this.total$ = service.total$;

  }
  NUMBEROFRECORDS:any;
  countries$: Observable<Country[]>;
  total$: Observable<number>;
  closeResult: string;
  instructions = '';
  catfilename = '';
  _instruction = '';
  _catfile = '';
  public _subission = null;
  public _lobs = null;
  public _catAnalysts = null;
  public _catManagers = null;
  public _userList  = null;
  public totalSubmissions : string;
  rowdata: Observable<any>;
  AccountTabledata:any;
  AccountTableheader:any;
  LocationsTabledata:any;
  LocationsTableheader:any;
  expirationDate: NgbDateStruct;
  inceptionDate: NgbDateStruct;
  date: { year: number, month: number };
  CurrentRefId:string;
  @ViewChild('catfileUploader', {static: false}) catfileUploader: ElementRef;
  @ViewChild('insfileUploader', {static: false}) insfileUploader: ElementRef;
  @ViewChild('dp', {static: false}) dp: NgbDatepicker;
  @ViewChild('assignmodal', {static: false}) assignmodal: TemplateRef<any>;
  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;


  onSort({column, direction}: SortEvent) {

    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }

  open(content, data, action,loc) {
    this.CurrentRefId = data.Ref_ID;
    if(loc == 'open') {
    this.showQCChecks = false;
    this.isAssigned = false;
    }
    if(loc == 'exec'){
      this.submission.readAccountOutputFiles(data.Ref_No).subscribe(resp => {
        this.AccountTabledata = resp;
        this.AccountTabledata = JSON.parse(this.AccountTabledata);
        this.AccountTableheader = this.AccountTabledata[0];
      });
      this.submission.readLocationsOutputFiles(data.Ref_No).subscribe(resp => {
        this.LocationsTabledata = resp;
        this.LocationsTabledata = JSON.parse(this.LocationsTabledata);
        this.LocationsTableheader = this.LocationsTabledata[0];
      });
    }
    this.modeofsubmission = action;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg', backdrop: 'static'}).result.then((result) => {
      this.showQCChecks = false;
      this.isAssigned = false;
      this.form.reset();
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.showQCChecks = false;
      this.isAssigned = false;
      this.form.reset();
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });

   // tslint:disable-next-line: align
   if (action === 'edit') {
    this.rowdata = data;
    this.form.controls.id.setValue(data.Ref_No);
    this.form.controls.policy_type.setValue(data.Policy_Type);
    this.form.controls.uw_name.setValue(data.UW_Name);
    this.form.controls.inception_date.setValue(data.Inception);
    this.form.controls.expire_date.setValue(data.Expiry);
    this.form.controls.tat.setValue(data.TAT);
    this.form.controls.comment.setValue(data.UW_Comments);
    this.form.controls.lob.setValue(data.LoB);
    this.form.controls.broker.setValue(data.Broker_Name);
    this.submission.getUserIdbyname(data.Cat_Analyst).subscribe(resp => {
      this.form.controls.assignanalyst.setValue(resp.id);
    });
    this.submission.getdocumentIds(data.Ref_No).subscribe(resp => {
      if(resp[0].path.indexOf('\\') > 0) {
      this.form.controls.SOVfile.setValue((resp[0].path).split('\\')[(resp[0].path).split('\\').length - 1]);
      this.form.controls.Instructionsfile.setValue((resp[1].path).split('\\')[(resp[1].path).split('\\').length - 1]);
      } else {
        this.form.controls.SOVfile.setValue((resp[0].path).split('/')[(resp[0].path).split('/').length - 1]);
        this.form.controls.Instructionsfile.setValue((resp[1].path).split('/')[(resp[1].path).split('/').length - 1]);

      }
    });
   } else if (action === 'view') {
    this.rowdata = data;
    this.form.controls.id.setValue(data.Ref_No);

    this.form.controls.assignanalyst.setValue(data.Cat_Analyst);

    this.submission.getresults(data.Ref_No).subscribe(resp => {
      this.form.controls.sov_file_validation.setValue(resp[0].qc_check);
      this.form.controls.duplicate_validation.setValue(resp[0].duplicate_check);
      this.form.controls.geo_validation.setValue(resp[0].geocode_check);
      this.form.controls.output_validation.setValue(resp[0].geocode_check);
      this.form.controls.subline.setValue(resp[0].subjectline);
      this.form.controls.cacomments.setValue(resp[0].ca_comments);
      this.form.controls.email_id.setValue(resp[0].email_id);
      this.form.controls.email_body.setValue(resp[0].email_body);
    });

    this.submission.getdocumentIds(data.Ref_No).subscribe(resp => {
      if(resp[0].path.indexOf('\\') > 0) {
      this.form.controls.SOVfile.setValue((resp[0].path).split('\\')[(resp[0].path).split('\\').length - 1]);
      this.form.controls.Instructionsfile.setValue((resp[1].path).split('\\')[(resp[1].path).split('\\').length - 1]);
      } else {
        this.form.controls.SOVfile.setValue((resp[0].path).split('/')[(resp[0].path).split('/').length - 1]);
        this.form.controls.Instructionsfile.setValue((resp[1].path).split('/')[(resp[1].path).split('/').length - 1]);

      }
    });
  } else {
    this.form.controls.id.setValue(data.Ref_No);
    this.submission.getUserIdbyname(data.Cat_Analyst).subscribe(resp => {
      this.form.controls.assignanalyst.setValue(resp.id);
    });

   }
    if (data.Modeling_Status === 'New') {
    var formData: any = new FormData();
    formData.append('sub_id', data.Ref_No);
    formData.append('updated_by', this.LoggedInUserId);
    formData.append('status', 'In Progress');
    this.submission.executeSubmission(formData).subscribe(resp => {
      this.getusersubmissionswithdates();
  });
  }
  }

  ApproveSubmission(data) {
    var formData: any = new FormData();
    formData.append('sub_id', data.Ref_No);
    formData.append('updated_by', this.LoggedInUserId);
    formData.append('status', 'Completed');
    this.submission.executeSubmission(formData).subscribe(resp => {
      this.getusersubmissionswithdates();
  });

  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
submitForm() {

    this.submitted = true;
    this.formErrors = false;
    if (this.form.invalid) {
      return;
    }

    var formData: any = new FormData();
    formData.append('id', this.form.get('id').value);
    formData.append('inception_date',this.form.get('inception_date').value);
    formData.append('expire_date', this.form.get('expire_date').value);
    formData.append('policy_type', this.form.get('policy_type').value);
    formData.append('tat', this.form.get('tat').value);
    formData.append('broker', this.form.get('broker').value);
    formData.append('lob', this.form.get('lob').value);
    formData.append('comment', this.form.get('comment').value);
    formData.append('updated_by', this.LoggedInUserId);
    if (this.form.get('SOVfile').value !== null) {
      formData.append('sovfile', this.form.get('SOVfile').value);
    }
    if (this.form.get('Instructionsfile').value !== null) {
      formData.append('instructionsfile', this.form.get('Instructionsfile').value);
    }
    if(this.form.get('SOVfile').value || this.form.get('Instructionsfile').value ) {
    this.submission.editSubmission(formData).subscribe(resp => {
      this.getusersubmissionswithdates();
    });
  }
    this.form.reset();
    this.modalService.dismissAll();
  }


openModellingFile(event: any, type) {
    if (type !== 'catfile') {
    let _catInstructfile : HTMLElement = document.getElementById('catinstructfile') as HTMLElement;
    _catInstructfile.click();
    } else {
      let _catmodellfile : HTMLElement = document.getElementById('catmodellfile') as HTMLElement;
      _catmodellfile.click();
    }

  }


executeSubmission(data) {
var formData: any = new FormData();
formData.append('sub_id', data.Ref_No);
formData.append('updated_by', this.LoggedInUserId);
formData.append('status', 'Executed');
this.submission.executeSubmission(formData).subscribe(resp => {
 // this.getusersubmissionswithdates();
 this.showQCChecks = true;
 this.QCCheckvariable = resp.Output_QC[0];
 this.duplicateCheck = resp.Output_Dup[0];
 this.GeoCodeCheck = resp.Output_Geo[0];
 this.outputFileCreation = resp.Output_AIR[0];

 if(this.duplicateCheck == 'Duplicate') {
  this.form.controls.subline.setValue('SOV_Number: '+data.Ref_No+' | SOV Submitted is Duplicate');
  this.form.controls.cacomments.setValue('');
  this.form.controls.email_id.setValue(data.UW_Name+'@sutherlandglobal.com');
  this.form.controls.email_body.setValue('1. Original SOV_Number:000000001'+
  '\n 2. Original SOV_LOB: Property'+
  '\n 3. Underwriter Name (Sender): UW-1'+
  '\n 4. Original SOV Received Date: 12/26/2019'+
  '\n 5. Original SOV Modeling Completed Date: 12/27/2019'+
  '\n 6. Earlier Model Output (AAL): $37,854');
  }
 if(this.QCCheckvariable.indexOf('Fail') > 0) {
    this.form.controls.subline.setValue('SOV_Number:'+data.Ref_No+'| Additional Details Required');
    this.form.controls.cacomments.setValue('');
    this.form.controls.email_id.setValue(data.UW_Name+'@sutherlandglobal.com');
    this.form.controls.email_body.setValue('Additional details required due to the below reasons \n'+
    '1. Locations contributing to 40% of total $TIV do not have street & Zip Code information \n'+
    'Please provide the above details and resubmit SOV to proceed with modeling');
    }

});


  }

onFileChange(event: any, type) {
    if (type !== 'catfile') {
    this.instructions = event.target.files[0].name;
    const file = (event.target as HTMLInputElement).files[0];
    this.form.patchValue({
      Instructionsfile : file
      });
    this.form.get('Instructionsfile').updateValueAndValidity();
     } else {
      this.catfilename = event.target.files[0].name;
      const file = (event.target as HTMLInputElement).files[0];
      this.form.patchValue({
        SOVfile : file
      });
      this.form.get('SOVfile').updateValueAndValidity();
    }
  }


applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

ngOnInit() {

   // this.data.currentMessage.subscribe(message => this.message = message);
    //alert("==2"+this.message);

    this.NUMBEROFRECORDS = sessionStorage.getItem('NUMBEROFRECORDS');
    this.LoggedInUserRole = sessionStorage.getItem('LoggedInUserRole');
    this.LoggedInUserId = sessionStorage.getItem('LoggedInUserId');
    this.LoggedInUserName = sessionStorage.getItem('LoggedInUserName');
    this.getusersubmissionswithdates();
    this._lobs = this.submission.getLobs();
    this._catAnalysts  = this.submission.getanalysts();
    this._userList = this.submission.getUserList();
    this._catManagers = this.submission.getManagerList();
  }
selectOutFileType(){
  var data = this.form.get('assignanalyst').value;
  if(data == this.LoggedInUserId) {
    this.isAssigned = true;
  }
  }

  sendEmail(){
    var formData: any = new FormData();
    formData.append('sub_id', this.form.get('id').value);
    formData.append('subline', this.form.get('subline').value);
    formData.append('cacomments', this.form.get('cacomments').value);
    formData.append('email_id', this.form.get('email_id').value);
    formData.append('email_body', this.form.get('email_body').value);
    this.submission.sendEmail(formData).subscribe(resp => {
      alert("Email Sent Successfully");
  });
  }



assigntoAnalyst() {
  var data = this.form.get('assignanalyst').value;

  var formData: any = new FormData();
  formData.append('sub_id', this.form.get('id').value);
  formData.append('updated_by', this.LoggedInUserId);
  formData.append('status', 'Assigned');
  formData.append('assigned_to', Number(data));
  this.submission.executeSubmission(formData).subscribe(resp => {
    this.getusersubmissionswithdates();
});
  //this.form.reset();

}

ngAfterViewInit() {
  this.state$ = this.activatedRoute.snapshot.paramMap.get('id');

  if(this.state$ != null){
    this.router.navigate(['/manager/submissionlist']);
    let currentTab='';
    this._subission.forEach(obj => {
        Object.entries(obj).forEach(([key, value]) => {
            if(`${value}` == this.state$){
              currentTab = obj;
            }
        });
      });
    this.open(this.assignmodal,currentTab,'assign','');
    }
  this.dataSource.paginator = this.paginator;
  this.dataSource.sort = this.sort;
}

  get f() {
    return this.form.controls;
  }

getusersubmissions(LoggedInUserId) {
     this.submission.getSubmissions(LoggedInUserId).subscribe(resp => {
     this._subission = resp;
     this.dataSource = new MatTableDataSource<Country>(this._subission);
    });

  }

  getusersubmissionswithdates() {
    this.dataservce.currentMessage.subscribe(message => this.message = message);
    var dates = this.message.split(',');
    this.submission.getSubmissionswithdates(this.LoggedInUserId,dates[0],dates[1]).subscribe(resp => {
      this._subission = resp;
      this.dataSource = new MatTableDataSource<Country>(this._subission);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
     });

 }
downloadFile(data, type) {
this.submission.getdocumentIds(data.Ref_No).subscribe(resp => {
  if (type === 'SOV') {
     window.location.href = this._url + '/filedownload/' + resp[0].document_id;
     } else if(type === 'Instructions') {
      window.location.href = this._url + '/filedownload/' + resp[1].document_id;
     } else if(type === 'Locations'){
      window.location.href = this._url + '/filedownload/' + resp[2].document_id;
     } else if(type === 'Accounts'){
      window.location.href = this._url + '/filedownload/' + resp[3].document_id;
     }
     });
  }

getCookie(key: string) {
    return this._cookieService.get(key);
  }


}
