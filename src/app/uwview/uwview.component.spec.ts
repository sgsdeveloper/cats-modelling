/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { UwviewComponent } from './uwview.component';

describe('UwviewComponent', () => {
  let component: UwviewComponent;
  let fixture: ComponentFixture<UwviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UwviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UwviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
