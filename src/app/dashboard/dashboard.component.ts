import { Component, OnInit, ViewChild } from '@angular/core';
import { CookieService } from 'ngx-cookie';
import { DashboardService } from '../services/dashboard.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { DataService } from '../services/data.service';
import {Observable, Subscription} from 'rxjs';
import { LocatioswithpieComponent } from '../shared/locatioswithpie/locatioswithpie.component';
import { LineofbusinessComponent } from '../shared/Lineofbusiness/Lineofbusiness.component';
import { LinechartComponent } from '../shared/linechart/linechart.component';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  TotalSubmitted: any;
  Awaiting_UW_Comment: any;
  Completed: any;
  Duplicate: any;
  Normal:any;
  Rush:any;
  message: string;
  LoggedInUserId: string;
  closeResult: string;
  subresult : any;
  clickEventsubscription:Subscription;
  startDate = 'All';
  endDate = 'All';
  TypeofFiles:any;
  @ViewChild(LocatioswithpieComponent, {static: false}) locPieComponent: LocatioswithpieComponent;
  @ViewChild(LineofbusinessComponent, {static: false}) loBComponent: LineofbusinessComponent;
  @ViewChild(LinechartComponent, {static: false}) lineComponent: LinechartComponent;

  constructor(private dashbordServicApi:DashboardService,  private _cookieService: CookieService,
              private modalService: NgbModal, private dataservce: DataService) {
    this.LoggedInUserId = sessionStorage.getItem("LoggedInUserId");
    this.clickEventsubscription=    this.dataservce.getClickEvent().subscribe(()=>{
      this.getDashBoardHeaders();
      })
    }

   getDashBoardHeaders(){
    this.dataservce.currentMessage.subscribe(message => this.message = message);
    var dates = this.message.split(',');
    this.startDate = dates[0];
    this.endDate = dates[1];
    this.dashbordServicApi.getUWdashboardHeaders(this.LoggedInUserId,'All','All','All',this.startDate, this.endDate).subscribe(data => {
      this.TotalSubmitted = data.TotalSubmitted;
      this.Awaiting_UW_Comment = data.Awaiting_UW_Comment;
      this.Completed = data.Completed;
      this.Duplicate = data.Duplicate;
      this.Normal = data.Normal;
      this.Rush = data.Rush;
    })
    this.locPieComponent.applyFilters(this.startDate,this.endDate);
    this.loBComponent.applyFilters(this.startDate,this.endDate);
    this.lineComponent.applyFilters(this.startDate,this.endDate);
   }

   open(content,typeoffiles) {
    this.TypeofFiles = typeoffiles;
    this.dashbordServicApi.getUWsubmissionsubset(this.LoggedInUserId,typeoffiles,this.startDate, this.endDate).subscribe(data => {
      this.subresult = data;
    });
    this.modalService.dismissAll();
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg', backdrop: 'static'}).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  ngOnDestroy() {
    this.modalService.dismissAll();
  }
  ngOnInit() {
    this.getDashBoardHeaders();
    $(document).ready(function() {
      $(".fullscreen-btn").click(function(e) {
        e.preventDefault();
        var $this = $(this);
        $this.children('i')
          .toggleClass('glyphicon-glyphicon-fullscreen')
          .toggleClass('glyphicon-glyphicon-remove');
        $(this).closest('.panel').toggleClass('panel-fullscreen');
       // var chartInfo = $this.attr("id") === 'panel-fullscreen' ? chart1Info : chart2Info;
       // console.log($this.id, chartInfo);

      });
      });

  }

  getCookie(key: string) {
    return this._cookieService.get(key);
  }

}
