import { DatePipe } from '@angular/common';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import moment from 'moment';
import { DaterangepickerConfig } from 'ng2-daterangepicker';
import { Router } from '@angular/router';
import { BroadcastService } from '../broadcast.service';
import { CookieService } from 'ngx-cookie';
import { DataService } from '../services/data.service';


@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  public startdate:string;
  public enddate:string;
  pipe = new DatePipe('en-US');

  public submission:boolean;
  public dashboard:boolean;
  public help:boolean;

  public chosenDate: any = {
    start: moment().subtract(12, 'month'),
    end: moment().subtract(0, 'month'),
  };

  public picker1 = {
    opens: 'left',
    startDate: moment().subtract(5, 'day'),
    endDate: moment(),
    isInvalidDate: function (date: any) {
      if (date.isSame('2017-09-26', 'day'))
        return 'mystyle';
      return false;
    }
  }



  constructor(private daterangepickerOptions: DaterangepickerConfig,
              public router: Router, private _cookieService: CookieService,
              private broadcastService: BroadcastService, private dataservice:DataService) {

    this.daterangepickerOptions.settings = {
      locale: { format: 'YYYY-MM-DD' },
      alwaysShowCalendars: false,
      "opens": "right",
      ranges: {
              'All': [moment(2010,'year'), moment()],
		          'Today': [moment(), moment()],
              'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
              'Last 7 Days': [moment().subtract(6, 'days'), moment()],
              'Last 30 Days': [moment().subtract(29, 'days'), moment()],
              'This Month': [moment().startOf('month'), moment().endOf('month')],
              'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			  /*
        'Last Month': [moment().subtract(1, 'month'), moment()],
        'Last 3 Months': [moment().subtract(4, 'month'), moment()],
        'Last 6 Months': [moment().subtract(6, 'month'), moment()],
        'Last 12 Months': [moment().subtract(12, 'month'), moment()],
		*/
      }
    };

   }

  ngOnInit() {

    if(this.router.url.indexOf('submission') > 0 ){
      this.navigate(1);
    }else if(this.router.url.indexOf('dashboard') > 0 ){
      this.navigate(2);
    }else{
        this.navigate(3);
    }
    this.startdate = this.pipe.transform(this.chosenDate.start, 'yyyy-MM-dd');
    this.enddate = this.pipe.transform(this.chosenDate.end, 'yyyy-MM-dd');
    this.dataservice.changeMessage(this.startdate+","+this.enddate) ;
    //this.dataservice.changeMessage("2019-10-30,2020-04-30");
   // this.dataservice.sendClickEvent();
  }


 selectedDate(value: any, dateInput: any) {
  console.log(value);
  dateInput.start = value.start;
  dateInput.end = value.end;

  this.startdate = this.pipe.transform(dateInput.start, 'yyyy-MM-dd');
  this.enddate = this.pipe.transform(dateInput.end, 'yyyy-MM-dd');
  this.dataservice.changeMessage(this.startdate+","+this.enddate) ;
  this.dataservice.sendClickEvent();
}

 calendarEventsHandler(e: any) {
  console.log({ calendarEvents: e });
}

 public applyDatepicker(e: any) {
  console.log({ applyDatepicker: e });
}

updateSettings(){
  this.daterangepickerOptions.settings.locale = { format: 'YYYY/MM/DD' };
  this.daterangepickerOptions.settings.ranges = {
    '30 days ago': [moment().subtract(1, 'month'), moment()],
    '3 months ago': [moment().subtract(4, 'month'), moment()],
    '6 months ago': [moment().subtract(6, 'month'), moment()],
    '7 months ago': [moment().subtract(12, 'month'), moment()],
  };
}

getCookie(key: string) {
  return this._cookieService.get(key);
}

navigate(data){
  this.submission = false;
  this.dashboard = false;
  this.help = false;
  if(data == 1){
    this.submission = true;
  }else if(data == 2){
    this.dashboard = true;
  }else if(data == 3){
    this.help = true;
  }
}

}
