import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-sitesettings',
  templateUrl: './sitesettings.component.html',
  styleUrls: ['./sitesettings.component.css']
})
export class SitesettingsComponent implements OnInit {
  LoggedInUserId:string;
  LoggedInUserRole:string;
  form: FormGroup;
  constructor(private _cookieService: CookieService,
    private loginServiceApi:LoginService, public fb: FormBuilder, public router: Router) {

      this.form = this.fb.group({
        numberofrecords: ['']
      });
     }

  ngOnInit() {
    this.LoggedInUserId = sessionStorage.getItem("LoggedInUserId");
    this.LoggedInUserRole = sessionStorage.getItem("LoggedInUserRole");
     this.form.controls.numberofrecords.setValue(sessionStorage.getItem("NUMBEROFRECORDS"));

  }

  getCookie(key: string) {
    return this._cookieService.get(key);
  }
  changeRecords(){
    var formData: any = new FormData();
    formData.append('user_id', this.LoggedInUserId);
    formData.append('numberofrecords', this.form.get('numberofrecords').value);
    this.loginServiceApi.changeRecords(formData).subscribe(data => {
      alert("Changed Successfully");
      this._cookieService.put('NUMBEROFRECORDS', this.form.get('numberofrecords').value);
      sessionStorage.setItem('NUMBEROFRECORDS', this.form.get('numberofrecords').value);
    });
  }
  gotoHome(){
    if(this.LoggedInUserRole === 'CAT_MANAGER'){
    this.router.navigate(['/manager']);
    }else if(this.LoggedInUserRole === 'ANALYST'){
      this.router.navigate(['/analyst']);
    }else{
      this.router.navigate(['/home']);
    }

  }
}
