import { Component, OnInit, ViewChild } from '@angular/core';
import { CookieService } from 'ngx-cookie';
import { DashboardService } from '../services/dashboard.service';
import { DataService } from '../services/data.service';
import { SubmissionService } from '../services/submission.service';
import {Observable, Subscription} from 'rxjs';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgbDateStruct, NgbCalendar, NgbDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup } from '@angular/forms';
import { LocationsComponent } from '../shared/Locations/Locations.component';
import { PiechartComponent } from '../shared/piechart/piechart.component';
import { PerformanceofcompletedaccountComponent } from '../shared/performanceofcompletedaccount/performanceofcompletedaccount.component';

@Component({
  selector: 'app-managerdashboard',
  templateUrl: './managerdashboard.component.html',
  styleUrls: ['./managerdashboard.component.css']
})
export class ManagerdashboardComponent implements OnInit {
  Awaiting_Analyst_Action: any;
  Awaiting_UW_Input: any;
  CATS_in_progress: any;
  Completed: any;
  RMS_AIR:any;
  Unassigned:any;
  LoggedInUserId: string;
  duplicateAccounts:any;
  duplicateAccountsNumber:any;
  public _lobs = null;
  public _userList = null;
  public _managerList = null;
  public _analystList = null;
  closeResult: string;
  private _subission: Object;
  clickEventsubscription:Subscription;
  message: string;
  subresult:any;
  form: FormGroup;
  startDate = 'All';
  endDate = 'All';
  TypeofFiles:any;
  @ViewChild(LocationsComponent, {static: false}) locComponent: LocationsComponent;
  @ViewChild(PiechartComponent, {static: false}) pieComponent: PiechartComponent;
  @ViewChild(PerformanceofcompletedaccountComponent, {static: false}) perComAccount: PerformanceofcompletedaccountComponent;


  constructor(public fb: FormBuilder,private dashbordServicApi:DashboardService,  private _cookieService: CookieService,private submission: SubmissionService,
              private dataservce: DataService, private modalService: NgbModal,private calendar: NgbCalendar,) {

                this.form = this.fb.group({
                  uw_name: [''],
                  lob_name: [''],
                  CatAnalyst_name: [''],
                  selected_date: [''],
                });

                this.clickEventsubscription=    this.dataservce.getClickEvent().subscribe(()=>{
                  this.dataservce.currentMessage.subscribe(message => this.message = message);

                  var dates = this.message.split(',');
                  this.startDate = dates[0];
                  this.endDate = dates[1];
                  this.dashbordServicApi.getUWdashboardHeaders(this.LoggedInUserId,'All','All','All',this.startDate, this.endDate).subscribe(data => {
                    this.Awaiting_Analyst_Action = data.Awaiting_Analyst_Action;
                    this.Awaiting_UW_Input = data.Awaiting_UW_Input;
                    this.CATS_in_progress = data.CATS_in_progress;
                    this.Completed = data.Completed;
                    this.RMS_AIR = data.RMS_AIR;
                    this.Unassigned = data.Unassigned;
                    this.dashbordServicApi.getDuplicateAccountProportion(this.LoggedInUserId,'All','All','All',this.startDate, this.endDate).subscribe(res => {
                      if(data.Completed != 0){
                      this.duplicateAccounts = ((res.count / data.Completed)*100).toFixed(2);
                      }else{
                        this.duplicateAccounts = 0 ;
                      }
                      this.duplicateAccountsNumber = res.count;
                    })

                  });
                  this.onFiltersChage();
                  });


                this.LoggedInUserId = sessionStorage.getItem("LoggedInUserId");
                this._lobs = this.submission.getLobs();
                this._userList = this.submission.getUserList();
                this._managerList = this.submission.getManagerList();
                this._analystList = this.submission.getanalysts();



                this.form.controls.uw_name.setValue('All');
                this.form.controls.lob_name.setValue('All');
                this.form.controls.CatAnalyst_name.setValue('All');

   }

   onFiltersChage(){
    this.dataservce.currentMessage.subscribe(message => this.message = message);
    var dates = this.message.split(',');
    this.startDate = dates[0];
    this.endDate = dates[1];

    let uw_name = this.form.get('uw_name').value;
    let lob_name = this.form.get('lob_name').value;
    let CatAnalyst_name = this.form.get('CatAnalyst_name').value;
    this.locComponent.applyFilters(uw_name,lob_name,CatAnalyst_name,this.startDate,this.endDate);
    this.pieComponent.applyFilters(uw_name,lob_name,CatAnalyst_name,this.startDate,this.endDate);
    this.perComAccount.applyFilters(uw_name,lob_name,CatAnalyst_name,this.startDate,this.endDate);

    this.dashbordServicApi.getUWdashboardHeaders(this.LoggedInUserId,uw_name,lob_name,CatAnalyst_name,this.startDate, this.endDate).subscribe(data => {
      this.Awaiting_Analyst_Action = data.Awaiting_Analyst_Action;
      this.Awaiting_UW_Input = data.Awaiting_UW_Input;
      this.CATS_in_progress = data.CATS_in_progress;
      this.Completed = data.Completed;
      this.RMS_AIR = data.RMS_AIR;
      this.Unassigned = data.Unassigned;
      this.dashbordServicApi.getDuplicateAccountProportion(this.LoggedInUserId,uw_name,lob_name,CatAnalyst_name,this.startDate, this.endDate).subscribe(res => {
        if(data.Completed != 0){
          this.duplicateAccounts = ((res.count / data.Completed)*100).toFixed(2);
          }else{
            this.duplicateAccounts = 0 ;
          }
        this.duplicateAccountsNumber = res.count;
          })
})
   }

   getCookie(key: string) {
    return this._cookieService.get(key);
  }
  ngOnDestroy() {
    this.modalService.dismissAll();
  }
  open(content,typeoffiles) {
    let uw_name = this.form.get('uw_name').value;
    let lob_name = this.form.get('lob_name').value;
    let CatAnalyst_name = this.form.get('CatAnalyst_name').value;
    this.TypeofFiles = typeoffiles;
    this.dataservce.currentMessage.subscribe(message => this.message = message);
    var dates = this.message.split(',');
    this.startDate = dates[0];
    this.endDate = dates[1];
    this.dashbordServicApi.getCMsubmissionsubset(this.LoggedInUserId,typeoffiles,uw_name,lob_name,CatAnalyst_name,this.startDate,this.endDate).subscribe(data => {
      this.subresult = data;
    });

    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', size: 'lg', backdrop: 'static'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else      if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  ngOnInit() {
    this.dataservce.currentMessage.subscribe(message => this.message = message);

    var dates = this.message.split(',');
    this.startDate = dates[0];
    this.endDate = dates[1];
    this.dashbordServicApi.getUWdashboardHeaders(this.LoggedInUserId,'All','All','All',this.startDate, this.endDate).subscribe(data => {
                        this.Awaiting_Analyst_Action = data.Awaiting_Analyst_Action;
                        this.Awaiting_UW_Input = data.Awaiting_UW_Input;
                        this.CATS_in_progress = data.CATS_in_progress;
                        this.Completed = data.Completed;
                        this.RMS_AIR = data.RMS_AIR;
                        this.Unassigned = data.Unassigned;
                        this.dashbordServicApi.getDuplicateAccountProportion(this.LoggedInUserId,'All','All','All',this.startDate, this.endDate).subscribe(res => {
                          if(data.Completed != 0){
                            this.duplicateAccounts = ((res.count / data.Completed)*100).toFixed(2);
                            }else{
                              this.duplicateAccounts = 0 ;
                            }
                          this.duplicateAccountsNumber = res.count;
                            })
          })

    $(document).ready(function() {
      $(".fullscreen-btn").click(function(e) {
        e.preventDefault();

        var $this = $(this);
        $this.children('i')
          .toggleClass('glyphicon-glyphicon-fullscreen')
          .toggleClass('glyphicon-glyphicon-remove');
        $(this).closest('.panel').toggleClass('panel-fullscreen');

      });
      });

  }

}


