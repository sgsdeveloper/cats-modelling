import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {environment} from '../../../src/environments/environment';
import { tap } from 'rxjs/internal/operators/tap';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { of } from 'rxjs/internal/observable/of';

@Injectable({
  providedIn: 'root'
})
export class SubmissionService {



  private _url = environment.apiUrl;
  private submissionurl = this._url + '/submissions';
  private data: any;

  constructor(private http: HttpClient) {


   }

  getSubmissions(user_id){
     return this.http.get(this.submissionurl+'?user_id='+user_id);
  }
  getSubmissionswithdates(user_id,stdate,enddate){
    return this.http.get(this.submissionurl+'?user_id='+user_id+'&stdate='+stdate+'&enddate='+enddate);
 }

  getLobs(){
    return this.http.get(this._url+'/getLOB');
 }



 getanalysts(): any {
  return this.http.get(this._url+'/getanalysts');
}

 getUserList(){
  return this.http.get(this._url+'/getUserList');
 }
 getManagerList(){
  return this.http.get(this._url+'/getManagerList');
 }

 getdocumentIds(submission_id){
  return this.http.get(this._url+'/getdocumentids?submission_id='+submission_id);
}
getresults(submission_id){
  return this.http.get(this._url+'/getresults?submission_id='+submission_id);
}

readAccountOutputFiles(sub_id){
  return this.http.get(this._url+'/readAccountOutputFiles?sub_id='+sub_id);
}
readLocationsOutputFiles(sub_id){
  return this.http.get(this._url+'/readLocationsOutputFiles?sub_id='+sub_id);
}

getUserIdbyname(user_name){
  return this.http.get<any>(this._url+'/getUserIdByName/'+user_name);
}

downloadFileApi(document_id){
  return this.http.get(this._url+'/filedownload/'+document_id,{ responseType: 'blob' });
}

 addNewSubmission(formData){
  return this.http.post<any>(this._url+'/submission', formData).pipe(
    tap(data => {
     }),
    catchError(this.handleError('addNewSubmission', []))
  );
 }
 addBindersSubmission(formData){
  return this.http.post<any>(this._url+'/binderssubmission', formData).pipe(
    tap(data => {
     }),
    catchError(this.handleError('binderssubmission', []))
  );
 }



 executeSubmission(id) {
   return this.http.post<any>(this._url+'/executesubmission', id).pipe(
    tap(data => {
     }),
    catchError(this.handleError('executesubmission', []))
  );
}
sendEmail(formData){
  return this.http.post<any>(this._url+'/sendEmail', formData).pipe(
    tap(data => {
     }),
    catchError(this.handleError('sendEmail', []))
  );
}

 editSubmission(formData){
   console.log(formData);
   return this.http.post<any>(this._url+'/editsubmission', formData).pipe(
    tap(data => {
     }),
    catchError(this.handleError('editSubmission', []))
  );
 }

 private handleError<T>(operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {
    return of(result as T);
  };
}

}

