import { Injectable } from '@angular/core';
import { Observable } from '../../../node_modules/rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import {environment} from '../../../src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private _url = environment.apiUrl;
  private LoginUrl = this._url + '/login';
  private data: any;


  constructor(private http: HttpClient) { }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(result as T);
    };
  }
  checkLogin(data): Observable<any> {
    return this.http.post<any>(this.LoginUrl, data)
      .pipe(
        tap(data => {
         }),
        catchError(this.handleError('getLoginDetails', []))
      );
  }

  getUserDetails(user_id){
    return this.http.get<any>(this._url + '/getUserProfile?user_id=' + user_id);
  }

  updateUserDetails(data){
    return this.http.post<any>(this._url + '/updateUserProfile', data)
    .pipe(
      tap(data => {
       }),
      catchError(this.handleError('updateUserProfile', []))
    );
  }

  getchangePassword(password){
    return this.http.post<any>(this._url + '/changePassword', password)
      .pipe(
        tap(data => {
         }),
        catchError(this.handleError('getchangePassword', []))
      );
  }

  changeRecords(data){
    return this.http.post<any>(this._url + '/changeRecords', data)
      .pipe(
        tap(data => {
         }),
        catchError(this.handleError('changeRecords', []))
      );
  }

  signInEvent(data): Observable<any> {
    return this.http.get<any>(this.LoginUrl + data)
      .pipe(
        tap(data => {
         }),
        catchError(this.handleError('signInEvent', []))
      );
  }

  setData(data: any) {
    this.data = data;
  }

  getData() {
    return this.data;
  }
}
