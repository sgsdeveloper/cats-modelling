import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private subject = new Subject<any>();

  private messageSource = new BehaviorSubject('default message');
  currentMessage = this.messageSource.asObservable();
  constructor() { }

changeMessage(message: string) {
  this.messageSource.next(message);
}

sendClickEvent() {
  this.subject.next();
}
getClickEvent(): Observable<any>{
  return this.subject.asObservable();
}

}
