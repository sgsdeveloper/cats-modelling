import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import {environment} from '../../../src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {


  private _url = environment.apiUrl;

  constructor(private http: HttpClient) { }

getUWdashboardHeaders(user_id,uw_name,lob_name,CatAnalyst_name,st_date,end_date) {
  // tslint:disable-next-line: max-line-length
  return this.http.get<any>(this._url+'/getUWdashboardHeaders?user_id='+user_id+'&uw_name='+uw_name+'&lob_name='+lob_name+'&CatAnalyst_name='+CatAnalyst_name+'&st_date='+st_date+'&end_date='+end_date);
}

getUWaccountsbylocation(user_id,st_date,end_date) {
  return this.http.get<any>(this._url+'/getUWaccountsbylocation?user_id='+user_id+'&st_date='+st_date+'&end_date='+end_date);
}

getCMaccountsbylocation(user_id,uw_name,lob_name,CatAnalyst_name,st_date,end_date) {
  return this.http.get<any>(this._url+'/getCMaccountsbylocation?user_id='+user_id+'&uw_name='+uw_name+'&lob_name='+lob_name+'&CatAnalyst_name='+CatAnalyst_name+'&st_date='+st_date+'&end_date='+end_date);
}

getUWaccountsbylocationdrilldown(user_id,option,st_date,end_date){
  return this.http.get<any>(this._url+'/getUWaccountsbylocationdrilldown?user_id='+user_id+'&option='+option+'&st_date='+st_date+'&end_date='+end_date);
}
getUWbylineofbusiness(user_id,st_date,end_date) {
  return this.http.get<any>(this._url+'/getUWbylineofbusiness?user_id='+user_id+'&st_date='+st_date+'&end_date='+end_date);
}
getmapdata(user_id,selectedPeril) {
  return this.http.get<any>(this._url+'/getmapdata?user_id='+user_id+'&peril='+selectedPeril);
}
getperilcurvedata(user_id,st_date,end_date) {
  return this.http.get<any>(this._url+'/getperilcurvedata?user_id='+user_id+'&st_date='+st_date+'&end_date='+end_date);
}
getpiechartdata(user_id,uw_name,lob_name,CatAnalyst_name,st_date,end_date) {
  return this.http.get<any>(this._url+'/getAccountsByTurnAround?user_id='+user_id+'&uw_name='+uw_name+'&lob_name='+lob_name+'&CatAnalyst_name='+CatAnalyst_name+'&st_date='+st_date+'&end_date='+end_date);
}
getDuplicateAccountProportion(user_id,uw_name,lob_name,CatAnalyst_name,st_date,end_date){
  return this.http.get<any>(this._url+'/getDuplicateAccountProportion?user_id='+user_id+'&uw_name='+uw_name+'&lob_name='+lob_name+'&CatAnalyst_name='+CatAnalyst_name+'&st_date='+st_date+'&end_date='+end_date);
}
getCMPerformanceCompletedAccount(user_id,uw_name,lob_name,CatAnalyst_name,st_date,end_date){
  return this.http.get<any>(this._url+'/getCMPerformanceCompletedAccount?user_id='+user_id+'&uw_name='+uw_name+'&lob_name='+lob_name+'&CatAnalyst_name='+CatAnalyst_name+'&st_date='+st_date+'&end_date='+end_date);
}
getCMPerformanceCompletedAccountDrillDown(user_id,seltype,uw_name,lob_name,CatAnalyst_name,st_date,end_date){
  return this.http.get<any>(this._url+'/getCMPerformanceCompletedAccountDrillDown?user_id='+user_id+'&seltype='+seltype+'&uw_name='+uw_name+'&lob_name='+lob_name+'&CatAnalyst_name='+CatAnalyst_name+'&st_date='+st_date+'&end_date='+end_date);
}

getCMsubmissionsubset(user_id,filter,uw_name,lob_name,CatAnalyst_name,st_date,end_date){
  return this.http.get<any>(this._url+'/getCMsubmissionsubset?user_id='+user_id+'&filter='+filter+'&uw_name='+uw_name+'&lob_name='+lob_name+'&CatAnalyst_name='+CatAnalyst_name+'&st_date='+st_date+'&end_date='+end_date);
}

getUWsubmissionsubset(user_id,filter,st_date,end_date){
  return this.http.get<any>(this._url+'/getUWsubmissionsubset?user_id='+user_id+'&filter='+filter+'&st_date='+st_date+'&end_date='+end_date);
}

}
