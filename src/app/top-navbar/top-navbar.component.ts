import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { BroadcastService } from '../broadcast.service';
import { CookieService } from 'ngx-cookie';

@Component({
  selector: 'app-top-navbar',
  templateUrl: './top-navbar.component.html',
  styleUrls: ['./top-navbar.component.css']
})
export class TopNavbarComponent implements OnInit {
  LoggedInUserName: string;
  role:string;
  LoggedInUserRole: string;

  constructor( private router: Router, private _cookieService: CookieService, private broadcastService: BroadcastService) { }

  ngOnInit() {
    this.LoggedInUserName = sessionStorage.getItem("LoggedInUserName");
    this.LoggedInUserRole = sessionStorage.getItem("LoggedInUserRole");
  }

  onLogout() {
    this.broadcastService.confirmLoggedIn({ 'isUserLoggedIn': false });
    this._cookieService.removeAll();
    localStorage.clear();
    this._cookieService.put('userLoggineIn', 'false');
    this.router.navigate(['/login']);
  }

  getCookie(key: string) {
    return this._cookieService.get(key);
  }

}
