import { Component, OnInit, ViewChild, HostListener, OnDestroy } from '@angular/core';
import { BroadcastService } from './broadcast.service';
import { CookieService } from 'ngx-cookie';
import { Router, NavigationEnd } from '@angular/router';
import { PathLocationStrategy, Location, LocationStrategy } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';
import { Keepalive } from '@ng-idle/keepalive';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [Location, { provide: LocationStrategy, useClass: PathLocationStrategy }],

})
export class AppComponent implements OnInit {
  title = 'cats-modelling';
  isUserLoggedIn: boolean = false;
  idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;
  data = [];

  constructor(private idle: Idle, private keepalive: Keepalive, private activeRoute: ActivatedRoute,
    private broadcastService: BroadcastService, private _cookieService: CookieService,
    private router: Router, location: LocationStrategy
  ) {


    /*
    let cookeyValue = localStorage.getItem('userLoggineIn');
    if (cookeyValue && cookeyValue === 'true') {
      this.isUserLoggedIn = true;
    }
*/



    // ========Code for idle time out =============

    // sets an idle timeout of 5 seconds, for testing purposes.
    idle.setIdle(540);
    // sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
    idle.setTimeout(60);
    // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
    idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    idle.onIdleEnd.subscribe(() => {
      this.idleState = 'No longer idle.';
      this.reset();
    });

    idle.onTimeout.subscribe(() => {
      this.idleState = 'Timed out!';
      this.timedOut = true;
      localStorage.clear();
      sessionStorage.clear();
      this.broadcastService.confirmLoggedIn({ 'isUserLoggedIn': false });
      this._cookieService.removeAll();
      this.router.navigate(['/']);
      this.reset();
    });

    idle.onIdleStart.subscribe(() => {
      this.idleState = 'You\'ve gone idle!';
    });

    idle.onTimeoutWarning.subscribe((countdown: string) => {
      this.idleState = 'You will time out in ' + countdown + ' seconds!';
    });

    // sets the ping interval to 15 seconds
    keepalive.interval(15);

    keepalive.onPing.subscribe(() => this.lastPing = new Date());

    this.reset();



    //========= END ===========

    broadcastService.loginAnnounced$.subscribe(
      value => {

       // this.isUserLoggedIn = value.isUserLoggedIn;

      });

    location.onPopState(() => {
      let locationPath = location.path();
      if (locationPath === '/' || locationPath === '/login') {
      }
    });
  }

  reset() {
    this.idle.watch();
    this.idleState = 'Started.';
    this.timedOut = false;
  }

  ngOnInit() {
    let cookeyValue = sessionStorage.getItem('userLoggineIn');
    if (cookeyValue && cookeyValue === 'true') {
      this.isUserLoggedIn = true;
    }

  }
  getCookie(key: string) {
    return this._cookieService.get(key);
  }

}
