export interface Country {
  ref_id:string;
  sub_date:string;
  sub_time:string;
  inception_date:string;
  expire_date:string;
  UWName:string;
  broker:string;
  lob:string;
  tat:string;
  duplicate:string;
  status:string;
  submitted_at:string;
}
