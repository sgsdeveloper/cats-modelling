
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbPanelChangeEvent, NgbAccordion } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie';
@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.css']
})

export class HelpComponent implements OnInit {
  @ViewChild('myaccordion', { static: true }) accordion: NgbAccordion;
  sIde: any;
  isCatAnalyst:any;
  isCatManager:any;
  isUnderWriter:any;
  LoggedInUserRole:any;
  constructor(private _cookieService: CookieService,public router: Router) { }

  ngOnInit() {
    this.LoggedInUserRole = sessionStorage.getItem("LoggedInUserRole");
    this.isCatAnalyst = false;
    this.isCatManager = false;
    this.isUnderWriter = false;
    if(this.LoggedInUserRole == 'UNDERWRITER'){
      this.isUnderWriter = true;
    }else if(this.LoggedInUserRole == 'CAT_MANAGER'){
      this.isCatManager = true;
    }else if(this.LoggedInUserRole == 'ANALYST'){
      this.isCatAnalyst = true;
    }

  }
  getCookie(key: string) {
    return this._cookieService.get(key);
  }
  beforeChange($event: NgbPanelChangeEvent) {
    if (this.sIde !== $event.panelId) {
      if (this.sIde) { this.accordion.toggle(this.sIde) }
      this.sIde = $event.panelId

    }

  }

  togglePanel(id) {
    debugger
    this.accordion.toggle(id);
    debugger
  }
}
