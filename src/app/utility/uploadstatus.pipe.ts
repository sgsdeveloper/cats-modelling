import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'modelingstatus'
})
export class UploadstatusPipe implements PipeTransform {

  transform(value: any): any {
    console.log(value);
    if ( String(value).toLocaleLowerCase() === 'in progress') {

      return 'in_progress';
    }  else if ( value  === 'Bound') {
      return 'Bound';
    }else if ( value  === 'Not Bound') {
      return 'Not Bound';
    }else if(String(value).toLocaleLowerCase() === 'pending'){
      return 'pending_color';
    }else if ( String(value).toLocaleLowerCase() === 'details required' || String(value).toLocaleLowerCase() === 'executed') {
      return 'details_required';
    } else if ( String(value).toLocaleLowerCase() === 'completed') {
      return 'completed';
    } else if ( String(value).toLocaleLowerCase() === 'on hold') {
      return 'on_hold';
    }else if ( String(value).toLocaleLowerCase().indexOf("not") >= 0||
      String(value).toLocaleLowerCase().indexOf("successfull") >= 0 ||
      String(value).toLocaleLowerCase().indexOf("created") >= 0 ||
      String(value).toLocaleLowerCase().indexOf("pass") >= 0) {
      return 'Success';
    } else if ( String(value).toLocaleLowerCase().indexOf("fail") >= 0 ||
               String(value).toLocaleLowerCase() == "duplicate") {
      return 'Fail';
    }  else if ( value  === 'Blank/Modeling In Progress' || value === 'Awaiting UW Input' || value === 'Unassigned') {
      return '';
    } else if ( value  === 'Quoted-Rejected' || value === 'Unqoted-Rejected') {
      return 'Rejected';
    }
     else {
      return "";
    }

  }

}
