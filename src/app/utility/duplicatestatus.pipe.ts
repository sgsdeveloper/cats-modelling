import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'duplicate'
})
export class DuplicatestatusPipe implements PipeTransform {

  transform(value: any): any {
    if ( String(value).toLocaleLowerCase() === 'y') {
      return 'Yes';
    } else {
      return 'No';
    }
  }

}
