import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
//import { NgbdTableComplete } from './table-complete';
import { NgbdSortableHeader } from './sortable.directive';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SampleChartComponent } from './sample-chart/sample-chart.component';
import { SubmisionManagerComponent } from './submision-manager/submision-manager.component';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { HelpComponent } from './help/help.component';
import { MomentModule } from 'angular2-moment';
import { Daterangepicker } from 'ng2-daterangepicker';
import { TopNavbarComponent } from './top-navbar/top-navbar.component';
import { FooterComponent } from './footer/footer.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UwdashboardComponent } from './uwdashboard/uwdashboard.component';
import { AnalystdashboardComponent } from './analystdashboard/analystdashboard.component';
import { ManagerdashboardComponent } from './managerdashboard/managerdashboard.component';
import { UserlistComponent } from './userlist/userlist.component';
import { UseraccountComponent } from './useraccount/useraccount.component';
import { SitesettingsComponent } from './sitesettings/sitesettings.component';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { CookieModule } from 'ngx-cookie';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { RolelistComponent } from './rolelist/rolelist.component';
import { MyinfoComponent } from './myinfo/myinfo.component';
import { DataTablesModule } from 'angular-datatables';
import { UploadstatusPipe } from './utility/uploadstatus.pipe';
import { DuplicatestatusPipe } from './utility/duplicatestatus.pipe';
import { DatePipe } from '@angular/common';

//import { PieChartComponent } from './shared/pie-chart/pie-chart.component';
import { UwviewComponent } from './uwview/uwview.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material';
import { ManagerviewComponent } from './managerview/managerview.component';
import { ManagersubmissionComponent } from './managersubmission/managersubmission.component';
import { ManagernavbarComponent } from './managernavbar/managernavbar.component';
import { LocationsComponent } from './shared/Locations/Locations.component';
import { LineofbusinessComponent } from './shared/Lineofbusiness/Lineofbusiness.component';
import { PerilCurveComponent } from './shared/PerilCurve/PerilCurve.component';
import { LinechartComponent } from './shared/linechart/linechart.component';
import { PiechartComponent } from './shared/piechart/piechart.component';
import { StylePaginatorDirective } from './style-paginator.directive';
import { HashLocationStrategy, LocationStrategy  } from '@angular/common';
import { PerformanceofcompletedaccountComponent } from './shared/performanceofcompletedaccount/performanceofcompletedaccount.component';
import { LocatioswithpieComponent } from './shared/locatioswithpie/locatioswithpie.component';
import { AnalystviewComponent } from './analystview/analystview.component';
import { AnalystnavbarComponent } from './analystnavbar/analystnavbar.component';
import { AnalystsubmissionComponent } from './analystsubmission/analystsubmission.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SampleChartComponent,
    SubmisionManagerComponent,
    //NgbdTableComplete,
    NgbdSortableHeader,
    HelpComponent,
    TopNavbarComponent,
    FooterComponent,
    NavBarComponent,
    DashboardComponent,
    UwdashboardComponent,
    AnalystdashboardComponent,
    ManagerdashboardComponent,
    UserlistComponent,
    UseraccountComponent,
    SitesettingsComponent,
    ChangepasswordComponent,
    RolelistComponent,
    MyinfoComponent,
    UploadstatusPipe,
    DuplicatestatusPipe,
    UwviewComponent,
    ManagerviewComponent,
    ManagersubmissionComponent,
    ManagernavbarComponent,
    LocationsComponent,
    LineofbusinessComponent,
    PerilCurveComponent,
    LinechartComponent,
    PiechartComponent,
    StylePaginatorDirective,
    PerformanceofcompletedaccountComponent,
    LocatioswithpieComponent,
      AnalystviewComponent,
      AnalystnavbarComponent,
      AnalystsubmissionComponent
   ],
  imports: [
    NgIdleKeepaliveModule.forRoot(),
    BrowserModule,
    MomentModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    NgxDropzoneModule,
    CookieModule.forRoot(),
    Daterangepicker,
    DataTablesModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule
  ],
  providers: [DatePipe,{provide : LocationStrategy , useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
