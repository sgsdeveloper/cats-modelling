import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-myinfo',
  templateUrl: './myinfo.component.html',
  styleUrls: ['./myinfo.component.css']
})
export class MyinfoComponent implements OnInit {
  LoggedInUserId:string;
  LoggedInUserRole:string;
  form: FormGroup;
  editMode = false;
  constructor(private _cookieService: CookieService,
    private loginServiceApi:LoginService, public fb: FormBuilder, public router: Router) {

      this.form = this.fb.group({
        firstName: [''],
        lastName: [''],
        country: [''],
        productLine: ['']
      });
     }

  ngOnInit() {
    this.editMode = false;
    this.LoggedInUserId = sessionStorage.getItem("LoggedInUserId");
    this.LoggedInUserRole = sessionStorage.getItem("LoggedInUserRole");
    this.loginServiceApi.getUserDetails(this.LoggedInUserId).subscribe(data => {
      if(data.first_name != 'null')
      this.form.controls.firstName.setValue(data.first_name);
      if(data.last_name != 'null')
      this.form.controls.lastName.setValue(data.last_name);
      if(data.Geo != 'null')
      this.form.controls.country.setValue(data.Geo);
      if(data.product_line != 'null')
      this.form.controls.productLine.setValue(data.product_line);
    });
  }


  getCookie(key: string) {
    return this._cookieService.get(key);
  }
  updateProfile(){
    var formData: any = new FormData();
    formData.append('id',this.LoggedInUserId);
    formData.append('firstName',this.form.get('firstName').value);
    formData.append('lastName',this.form.get('lastName').value);
    formData.append('country',this.form.get('country').value);
    formData.append('productLine',this.form.get('productLine').value);
    this.loginServiceApi.updateUserDetails(formData).subscribe(data => {
      alert("Updated Successfully")
    });
  }
  gotoHome(){
    if(this.LoggedInUserRole === 'CAT_MANAGER'){
    this.router.navigate(['/manager']);
    }else if(this.LoggedInUserRole === 'ANALYST'){
      this.router.navigate(['/analyst']);
    }else{
      this.router.navigate(['/home']);
    }

  }
  editProfile(){
    this.editMode = true;
  }

}
