import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalystdashboardComponent } from './analystdashboard.component';

describe('AnalystdashboardComponent', () => {
  let component: AnalystdashboardComponent;
  let fixture: ComponentFixture<AnalystdashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnalystdashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalystdashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
