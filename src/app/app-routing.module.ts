import { AnalystsubmissionComponent } from './analystsubmission/analystsubmission.component';
import { ManagerviewComponent } from './managerview/managerview.component';
import { UwviewComponent } from './uwview/uwview.component';
import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { RouterModule, Routes } from '@angular/router'
import { LoginComponent } from './login/login.component'
import { SampleChartComponent } from './sample-chart/sample-chart.component';
import { SubmisionManagerComponent } from './submision-manager/submision-manager.component';
import { HelpComponent } from './help/help.component';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { RolelistComponent } from './rolelist/rolelist.component';
import { MyinfoComponent } from './myinfo/myinfo.component';
import { UserlistComponent } from './userlist/userlist.component';
import { UseraccountComponent } from './useraccount/useraccount.component';
import { SitesettingsComponent } from './sitesettings/sitesettings.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { from } from 'rxjs';
import { ManagersubmissionComponent } from './managersubmission/managersubmission.component';
import { ManagerdashboardComponent } from './managerdashboard/managerdashboard.component';
import { AnalystviewComponent } from './analystview/analystview.component';
import { AnalystdashboardComponent } from './analystdashboard/analystdashboard.component';



const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'sample', component: SampleChartComponent },



  { path: 'change-password', component: ChangepasswordComponent },
  { path: 'role-list', component: RolelistComponent },
  { path: 'my-info', component: MyinfoComponent },
  { path: 'user-list', component: UserlistComponent },
  { path: 'helps', component: UseraccountComponent },
  { path: 'settings', component: SitesettingsComponent },

  { path: 'home', component: UwviewComponent,

  children: [
     { path: '', redirectTo: 'submission', pathMatch: 'full' },
     { path: 'submission', component: SubmisionManagerComponent },
     { path: 'submission/:id', component: SubmisionManagerComponent },
     { path: 'dashboard', component: DashboardComponent },
     { path: 'help', component: HelpComponent }
  ]
  },
  { path: 'manager', component: ManagerviewComponent,

  children: [
     { path: '', redirectTo: 'submissionlist', pathMatch: 'full' },
     { path: 'submissionlist', component: ManagersubmissionComponent },
     { path: 'submissionlist/:id', component: ManagersubmissionComponent },
     { path: 'managerdashboard', component: ManagerdashboardComponent },
     { path: 'help', component: HelpComponent }
  ]
  },
  { path: 'analyst', component: AnalystviewComponent,

  children: [
     { path: '', redirectTo: 'submissions', pathMatch: 'full' },
     { path: 'submissions', component: AnalystsubmissionComponent },
     { path: 'submissions/:id', component: AnalystsubmissionComponent },
     { path: 'analystdashboard', component: AnalystdashboardComponent },
     { path: 'help', component: HelpComponent }
  ]
  },
  { path: '**', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
